// <copyright file="InstitutionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Class for Institution methods only.
    /// </summary>
    public class InstitutionRepository : GenericRepository<Institution>, IInstitutionRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionRepository"/> class.
        /// </summary>
        /// <param name="ctx">KutyatenyesztoDbContext type element.</param>
        public InstitutionRepository(KutyatenyesztoDbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Deletes one Institution with the matching id.
        /// </summary>
        /// <param name="id">int type element.</param>
        public void Delete(int id)
        {
            var institutions = this.context.Institutions.FirstOrDefault(institution => institution.InstitutionId == id);
            if (institutions != null)
            {
                this.context.Remove(institutions);
                this.context.SaveChanges();
            }
        }

        /// <summary>
        /// Returns one Institution with matching id.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Institution type element.</returns>
        public override Institution GetById(int id)
        {
            return this.context.Institutions.FirstOrDefault(institution => institution.InstitutionId == id);
        }

        /// <summary>
        /// Inserts a new Institution into the database.
        /// </summary>
        /// <param name="item">User type element.</param>
        public void Insert(Institution item)
        {
            this.context.Add(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Updates one Institution.
        /// </summary>
        /// <param name="item">Institution type element.</param>
        /// <param name="id">int type element.</param>
        public void Update(Institution item, int id)
        {
            var currentInstitution = this.context.Institutions.FirstOrDefault(institution => institution.InstitutionId == item.InstitutionId);
            if (currentInstitution != null && item != null)
            {
                currentInstitution.Name = item.Name;
                currentInstitution.Address = item.Address;
                currentInstitution.Director = item.Director;
                currentInstitution.Capacity = item.Capacity;
                currentInstitution.PhoneNumber = item.PhoneNumber;
                currentInstitution.Selected = item.Selected;
                this.context.SaveChanges();
            }
        }
    }
}