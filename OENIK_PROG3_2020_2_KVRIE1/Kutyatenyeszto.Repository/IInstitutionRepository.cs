// <copyright file="IInstitutionRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.ObjectModel;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Interface for institution only repository.
    /// </summary>
    public interface IInstitutionRepository : IRepository<Institution>
    {
        /// <summary>
        /// Deletes an Institution.
        /// </summary>
        /// <param name="id">int type element.</param>
        void Delete(int id);

        /// <summary>
        /// Inserts an Institution.
        /// </summary>
        /// <param name="item">Insitution type element.</param>
        void Insert(Institution item);

        /// <summary>
        /// Updates an Institution.
        /// </summary>
        /// <param name="item">Insitution type element.</param>
        /// <param name="id">int type element.</param>
        void Update(Institution item, int id);
    }
}