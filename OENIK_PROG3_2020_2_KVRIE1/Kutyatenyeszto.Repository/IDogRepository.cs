// <copyright file="IDogRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.ObjectModel;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Interface for dog only repository.
    /// </summary>
    public interface IDogRepository : IRepository<Dog>
    {
        /// <summary>
        /// Deletes a Dog.
        /// </summary>
        /// <param name="id">int type element.</param>
        void Delete(int id);

        /// <summary>
        /// Inserts a Dog.
        /// </summary>
        /// <param name="item">Dog type element.</param>
        void Insert(Dog item);

        /// <summary>
        /// Updates a Dog.
        /// </summary>
        /// <param name="item">Dog type element.</param>
        void Update(Dog item);
    }
}