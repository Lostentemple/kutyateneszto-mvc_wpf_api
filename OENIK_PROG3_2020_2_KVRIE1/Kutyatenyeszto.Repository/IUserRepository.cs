// <copyright file="IUserRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.ObjectModel;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Interface for user only repository.
    /// </summary>
    public interface IUserRepository : IRepository<User>
    {
        /// <summary>
        /// Deletes a User.
        /// </summary>
        /// <param name="id">int type element.</param>
        void Delete(int id);

        /// <summary>
        /// Inserts a new User.
        /// </summary>
        /// <param name="item">User type element.</param>
        void Insert(User item);

        /// <summary>
        /// Updates a User.
        /// </summary>
        /// <param name="item">User type element.</param>
        /// <param name="id">int type element.</param>
        void Update(User item, int id);
    }
}