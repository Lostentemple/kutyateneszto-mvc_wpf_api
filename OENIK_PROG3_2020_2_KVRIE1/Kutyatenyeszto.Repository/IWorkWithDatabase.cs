// <copyright file="IWorkWithDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    /// <summary>
    /// Interface for repository connections and saving changes.
    /// </summary>
    public interface IWorkWithDatabase
    {
        /// <summary>
        /// Gets connection to user repository.
        /// </summary>
        IUserRepository Users { get; }

        /// <summary>
        /// Gets connection to dog repository.
        /// </summary>
        IDogRepository Dogs { get; }

        /// <summary>
        /// Gets connection to institution repository.
        /// </summary>
        IInstitutionRepository Institutions { get; }

        /// <summary>
        /// Gets connection to user repository.
        /// </summary>
        IAdoptationRepository Adoptations { get; }

        /// <summary>
        /// Saving all changes.
        /// </summary>
        void Save();
    }
}