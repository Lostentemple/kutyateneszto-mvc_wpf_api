﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Design", "CA1051:Do not declare visible instance fields", Justification = "Had to be supressed", Scope = "member", Target = "~F:Kutyatenyeszto.Repository.GenericRepository`1.context")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("StyleCop.CSharp.MaintainabilityRules", "SA1401:Fields should be private", Justification = "Had to be supressed", Scope = "member", Target = "~F:Kutyatenyeszto.Repository.GenericRepository`1.context")]
[assembly: SuppressMessage("", "CA1014", Justification = "done", Scope = "module")]
