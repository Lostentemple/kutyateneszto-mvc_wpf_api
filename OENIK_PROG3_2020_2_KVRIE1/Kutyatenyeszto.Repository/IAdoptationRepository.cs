// <copyright file="IAdoptationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Interface for Adoptation repository.
    /// </summary>
    public interface IAdoptationRepository : IRepository<Adoptation>
    {
    }
}