// <copyright file="AdoptationRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Class for Adaptation methods only.
    /// </summary>
    public class AdoptationRepository : GenericRepository<Adoptation>, IAdoptationRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AdoptationRepository"/> class.
        /// </summary>
        /// <param name="ctx">KutyatenyesztoDbContext type element.</param>
        public AdoptationRepository(KutyatenyesztoDbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Returns an adaptation element by id.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Adatptation type element.</returns>
        public override Adoptation GetById(int id)
        {
            throw new System.NotImplementedException();
        }
    }
}