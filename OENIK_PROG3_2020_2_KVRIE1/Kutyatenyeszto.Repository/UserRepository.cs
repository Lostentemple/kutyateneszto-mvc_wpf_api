// <copyright file="UserRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Class for User methods only.
    /// </summary>
    public class UserRepository : GenericRepository<User>, IUserRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="UserRepository"/> class.
        /// </summary>
        /// <param name="ctx">KutyatenyesztoDbContext type element.</param>
        public UserRepository(KutyatenyesztoDbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Deletes one User with the matching id.
        /// </summary>
        /// <param name="id">int type element.</param>
        public void Delete(int id)
        {
            var users = this.context.Users.FirstOrDefault(user => user.UserId == id);
            if (users != null)
            {
                this.context.Remove(users);
                this.context.SaveChanges();
            }
        }

        /// <summary>
        /// Returns one User with matching id.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>User type element.</returns>
        public override User GetById(int id)
        {
            return this.context.Users.FirstOrDefault(user => user.UserId == id);
        }

        /// <summary>
        /// Inserts a new User into the database.
        /// </summary>
        /// <param name="item">User type element.</param>
        public void Insert(User item)
        {
            this.context.Add(item);
            this.context.SaveChanges();
        }

        /// <summary>
        /// Updates one User.
        /// </summary>
        /// <param name="item">User type element.</param>
        /// <param name="id">int type element.</param>
        public void Update(User item, int id)
        {
            var currentUser = this.context.Users.FirstOrDefault(user => user.UserId == item.UserId);
            if (currentUser != null && item != null)
            {
                currentUser.UserName = item.UserName;
                currentUser.Password = item.Password;
                currentUser.BirthDate = DateTime.Now;
                currentUser.Gender = item.Gender;
                currentUser.EmailAddress = item.EmailAddress;
                currentUser.PhoneNumber = item.PhoneNumber;
                this.context.SaveChanges();
            }
        }
    }
}