// <copyright file="WorkwithDatabase.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using Kutyatenyeszto.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Class for repository connections and saving changes.
    /// </summary>
    public class WorkwithDatabase : IWorkWithDatabase
    {
        /// <summary>
        /// Connection to user repository.
        /// </summary>
        private UserRepository users;

        /// <summary>
        /// Connection to dog repository.
        /// </summary>
        private DogRepository dogs;

        /// <summary>
        /// Connection to institution repository.
        /// </summary>
        private InstitutionRepository institutions;

        /// <summary>
        /// Connection to adoptation repository.
        /// </summary>
        private AdoptationRepository adoptations;

        /// <summary>
        /// Initializes a new instance of the <see cref="WorkwithDatabase"/> class.
        /// </summary>
        /// <param name="ctx">Dbcontext type element.</param>
        public WorkwithDatabase(KutyatenyesztoDbContext ctx)
        {
            this.Context = ctx;
        }

        /// <summary>
        /// Gets connection to database.
        /// </summary>
        public KutyatenyesztoDbContext Context { get; }

        /// <summary>
        /// Gets user repository connection.
        /// </summary>
        public IUserRepository Users
        {
            get
            {
                if (this.users == null)
                {
                    this.users = new UserRepository(this.Context);
                }

                return this.users;
            }
        }

        /// <summary>
        /// Gets user repository connection.
        /// </summary>
        public IDogRepository Dogs
        {
            get
            {
                if (this.dogs == null)
                {
                    this.dogs = new DogRepository(this.Context);
                }

                return this.dogs;
            }
        }

        /// <summary>
        /// Gets institution repository connection.
        /// </summary>
        public IInstitutionRepository Institutions
        {
            get
            {
                if (this.institutions == null)
                {
                    this.institutions = new InstitutionRepository(this.Context);
                }

                return this.institutions;
            }
        }

        /// <summary>
        /// Gets adoptation repository connection.
        /// </summary>
        public IAdoptationRepository Adoptations
        {
            get
            {
                if (this.adoptations == null)
                {
                    this.adoptations = new AdoptationRepository(this.Context);
                }

                return this.Adoptations;
            }
        }

        /// <summary>
        /// Saving all changes.
        /// </summary>
        public void Save()
        {
            this.Context.SaveChanges();
        }
    }
}