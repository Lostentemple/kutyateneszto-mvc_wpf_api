﻿// <copyright file="IRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;

    /// <summary>
    /// Interface which contains all inherited methods for all repositories.
    /// </summary>
    /// <typeparam name="T">T type element.</typeparam>
    public interface IRepository<T>
        where T : class
    {
        /// <summary>
        /// Get all items from given repository.
        /// </summary>
        /// <returns>IQueryable type element.</returns>
        IQueryable<T> GetAll();

        /// <summary>
        /// Get one item from given id from repository.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>T type element.</returns>
        T GetById(int id);
    }
}
