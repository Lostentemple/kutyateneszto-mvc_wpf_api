// <copyright file="DogRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.ObjectModel;
    using System.Linq;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Class for Dog methods only.
    /// </summary>
    public class DogRepository : GenericRepository<Dog>, IDogRepository
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="DogRepository"/> class.
        /// </summary>
        /// <param name="ctx">KutyatenyesztoDbContext type element.</param>
        public DogRepository(KutyatenyesztoDbContext ctx)
            : base(ctx)
        {
        }

        /// <summary>
        /// Deletes one Dog with the matching id.
        /// </summary>
        /// <param name="id">int type element.</param>
        public void Delete(int id)
        {
            var dogs = this.context.Dogs.FirstOrDefault(dog => dog.DogId == id);
            if (dogs != null)
            {
                this.context.Remove(dogs);
                this.context.SaveChanges();
            }

            // return dogs;
        }

        /// <summary>
        /// Deletes one or more Dog(s) with the matching id.
        /// </summary>
        /// <param name="ids">int type list.</param>
        /// <returns>Dog type list.</returns>
        public Collection<Dog> DeleteMore(Collection<int> ids)
        {
            if (ids == null)
            {
                return null;
            }

            var dogContext = this.context.Dogs;
            Collection<Dog> dogs = new Collection<Dog>();
            for (int i = 0; i < ids.Count; i++)
            {
                var dogEntity = dogContext.FirstOrDefault(dog => dog.DogId == ids[i]);
                dogs.Add(dogEntity);
            }

            this.context.RemoveRange(dogs);
            this.context.SaveChanges();
            return dogs;
        }

        /// <summary>
        /// Returns one Dog with matching id.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Dog type element.</returns>
        public override Dog GetById(int id)
        {
            return this.context.Dogs.FirstOrDefault(dog => dog.DogId == id);
        }

        /// <summary>
        /// Inserts a new Dog into the database.
        /// </summary>
        /// <param name="item">Dog type element.</param>
        public void Insert(Dog item)
        {
            this.context.Add(item);
            this.context.SaveChanges();

            // return this.context.Dogs.FirstOrDefault(dog => dog == item);
        }

        /// <summary>
        /// Inserts one ore more new Dog(s) into the database.
        /// </summary>
        /// <param name="itemList">Dog type list.</param>
        /// <returns>Dog type listt.</returns>
        public Collection<Dog> InsertMore(Collection<Dog> itemList)
        {
            var dogs = itemList;
            this.context.Dogs.AddRange(dogs);
            this.context.SaveChanges();
            return dogs;
        }

        /// <summary>
        /// Updates one Dog.
        /// </summary>
        /// <param name="item">Dog type element.</param>
        public void Update(Dog item)
        {
            var currentDog = this.context.Dogs.FirstOrDefault(dog => dog.DogId == item.DogId);
            if (currentDog != null)
            {
                if (item != null)
                {
                    currentDog.Age = item.Age;
                    currentDog.Breed = item.Breed;
                    currentDog.Name = item.Name;
                    currentDog.FurType = item.FurType;
                    currentDog.HasOwner = item.HasOwner;
                    currentDog.IsAccessible = item.IsAccessible;
                    currentDog.IsUncastrated = item.IsUncastrated;
                    this.context.SaveChanges();

                    // return this.context.Dogs.FirstOrDefault(dog => dog == item);
                }
            }

            // return null;
        }
    }
}