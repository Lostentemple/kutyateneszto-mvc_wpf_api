// <copyright file="GenericRepository.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Repository
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using Kutyatenyeszto.Data;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Class for generic repository methods.
    /// </summary>
    /// <typeparam name="T">T type element.</typeparam>
    public abstract class GenericRepository<T> : IRepository<T>
        where T : class
    {
        /// <summary>
        /// Gets connection to database.
        /// </summary>
        protected KutyatenyesztoDbContext context;

        /// <summary>
        /// Initializes a new instance of the <see cref="GenericRepository{T}"/> class.
        /// </summary>
        /// <param name="ctx">Dbcontext type element.</param>
        protected GenericRepository(KutyatenyesztoDbContext ctx)
        {
            this.context = ctx;
        }

        /// <summary>
        /// Get all items from given repository.
        /// </summary>
        /// <returns>T type IQueryable.</returns>
        public IQueryable<T> GetAll()
        {
            return this.context.Set<T>();
        }

        /// <summary>
        /// Get one item from given id from repository.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>T type element.</returns>
        public abstract T GetById(int id);
    }
}