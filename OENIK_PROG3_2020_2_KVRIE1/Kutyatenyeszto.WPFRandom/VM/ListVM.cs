﻿// <copyright file="ListVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPFRandom.VM
{
    using System.Collections.ObjectModel;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// ListViewModel class.
    /// </summary>
    internal class ListVM : ViewModelBase
    {
        private ObservableCollection<NewApiResult> newResults;
        private ObservableCollection<InstitutionVM> randomItemCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListVM"/> class.
        /// </summary>
        public ListVM()
        {
            this.newResults = new ObservableCollection<NewApiResult>();
            if (this.IsInDesignMode)
            {
                this.newResults.Add(new NewApiResult { OperationResult = true, ActionName = "Select", Count = 2 });
            }
        }

        /// <summary>
        /// Gets or sets newResults property.
        /// </summary>
        public ObservableCollection<NewApiResult> NewResults
        {
            get => this.newResults;
            set => this.Set(ref this.newResults, value);
        }

        /// <summary>
        /// Gets or sets randomItemCollection property.
        /// </summary>
        public ObservableCollection<InstitutionVM> RandomItemCollection
        {
            get => this.randomItemCollection;
            set => this.Set(ref this.randomItemCollection, value);
        }
    }
}
