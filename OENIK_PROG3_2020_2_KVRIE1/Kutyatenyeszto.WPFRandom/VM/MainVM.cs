﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPFRandom.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainVM class.
    /// </summary>
    internal class MainVM : ObservableObject
    {
        private MainLogic logic;
        private int generateNumber;
        private ObservableCollection<InstitutionVM> randomItemCollection;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            this.logic = new MainLogic();
            this.randomItemCollection = new ObservableCollection<InstitutionVM>();
            this.RandomCmd = new RelayCommand(() =>
            {
                Task randomInstitutions = new Task(
                    () =>
                {
                    for (int i = 0; i < this.generateNumber; i++)
                    {
                        var newItem = this.logic.GetOne().Result;
                        this.randomItemCollection.Add(newItem);
                    }
                }, TaskCreationOptions.LongRunning);
                randomInstitutions.Start();
                Task.WaitAll(randomInstitutions);
                ListWindow secondWindow = new ListWindow(this.randomItemCollection);
                secondWindow.ShowDialog();
            });
        }

        /// <summary>
        /// Gets or sets generateNumber property.
        /// </summary>
        public int GenerateNumber
        {
            get => this.generateNumber;
            set => this.Set(ref this.generateNumber, value);
        }

        /// <summary>
        /// Gets randomCmd property.
        /// </summary>
        public ICommand RandomCmd { get; private set; }

        /// <summary>
        /// Gets or sets randomItemCollection property.
        /// </summary>
        public ObservableCollection<InstitutionVM> RandomItemCollection
        {
            get => this.randomItemCollection;
            set => this.Set(ref this.randomItemCollection, value);
        }
    }
}
