﻿// <copyright file="ListWindow.xaml.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPFRandom
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows;
    using System.Windows.Controls;
    using System.Windows.Data;
    using System.Windows.Documents;
    using System.Windows.Input;
    using System.Windows.Media;
    using System.Windows.Media.Imaging;
    using System.Windows.Shapes;
    using System.Windows.Threading;
    using Kutyatenyeszto.WPFRandom.VM;

    /// <summary>
    /// Interaction logic for ListWindow.xaml.
    /// </summary>
    public partial class ListWindow : Window
    {
        private static readonly Random Random = new Random();
        private readonly MainLogic logic;
        private readonly ListVM listVM;
        private DispatcherTimer timer;

        /// <summary>
        /// Initializes a new instance of the <see cref="ListWindow"/> class.
        /// </summary>
        public ListWindow()
        {
            this.InitializeComponent();
            this.logic = new MainLogic();
            this.listVM = this.FindResource("VM") as ListVM;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="ListWindow"/> class.
        /// </summary>
        /// <param name="randomItemCollection">ObservableCollection type element.</param>
        public ListWindow(ObservableCollection<InstitutionVM> randomItemCollection)
            : this()
        {
            this.listVM.RandomItemCollection = randomItemCollection;
        }

        /// <summary>
        /// Gets timer property.
        /// </summary>
        public DispatcherTimer Timer { get => this.timer; }

        /// <summary>
        /// Gets randomItemCollection property.
        /// </summary>
        public ObservableCollection<InstitutionVM> RandomItemCollection { get => this.listVM.RandomItemCollection; }

        /// <summary>
        /// Gets newResults property.
        /// </summary>
        public ObservableCollection<NewApiResult> NewResults { get => this.listVM.NewResults; }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            this.timer = new DispatcherTimer();
            this.timer.Interval = TimeSpan.FromMilliseconds(2000);
            this.timer.Tick += this.TickEvent;
            this.timer.Start();
        }

        private async void TickEvent(object sender, EventArgs e)
        {
            NewApiResult newResult;
            InstitutionVM selectedInst = this.RandomItemCollection.ElementAt(Random.Next(this.RandomItemCollection.Count));
            if (selectedInst != null)
            {
                if (Random.Next(0, 2) == 0)
                {
                    newResult = await this.logic.Select(selectedInst);
                }
                else
                {
                    newResult = await this.logic.Unselect(selectedInst);
                }

                this.NewResults.Add(newResult);
            }
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            this.timer.Stop();
            foreach (var item in this.RandomItemCollection)
            {
                this.logic.ApiDelInstitution(item);
            }
        }
    }
}
