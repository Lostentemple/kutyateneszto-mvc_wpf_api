﻿// <copyright file="NewApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPFRandom
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;

    /// <summary>
    /// NewApiResult class.
    /// </summary>
    public class NewApiResult
    {
        /// <summary>
        /// Gets or sets a value indicating whether operationResult property.
        /// </summary>
        public bool OperationResult { get; set; }

        /// <summary>
        /// Gets or sets actionName property.
        /// </summary>
        public string ActionName { get; set; }

        /// <summary>
        /// Gets or sets count property.
        /// </summary>
        public int Count { get; set; }
    }
}
