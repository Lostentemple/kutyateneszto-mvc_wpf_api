﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPFRandom
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Kutyatenyeszto.WPFRandom.VM;
    using Newtonsoft.Json;

    /// <summary>
    /// MainLogic class.
    /// </summary>
    internal class MainLogic
    {
        private readonly string url = "https://localhost:44372/Random/";
        private readonly HttpClient client = new HttpClient();

        // private readonly JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// ApiDelInstitution method.
        /// </summary>
        /// <param name="institution">InstitutionVM type element.</param>
        public async void ApiDelInstitution(InstitutionVM institution)
        {
            bool success = false;
            if (institution != null)
            {
                string json = await this.client.GetStringAsync("https://localhost:44372/InstitutionsApi/del/" + institution.InstitutionId.ToString());
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }
        }

        /// <summary>
        /// GetOne method.
        /// </summary>
        /// <returns>Task type element.</returns>
        public async Task<InstitutionVM> GetOne()
        {
            var json = await this.client.GetStringAsync(this.url + "Getone");
            var list = JsonConvert.DeserializeObject<InstitutionVM>(json);
            return list;
        }

        /// <summary>
        /// Select method.
        /// </summary>
        /// <param name="inst">InstitutionVM type element.</param>
        /// <returns>Task type element.</returns>
        public async Task<NewApiResult> Select(InstitutionVM inst)
        {
            NewApiResult newResult = new NewApiResult() { OperationResult = false, ActionName = "Select", Count = 0 };
            if (inst != null)
            {
                string json = await this.client.GetStringAsync(this.url + "Select/" + inst.InstitutionId.ToString());
                var list = JsonConvert.DeserializeObject<NewApiResult>(json);
                newResult.OperationResult = list.OperationResult;
                newResult.Count = list.Count;
            }

            return newResult;
        }

        /// <summary>
        /// Unelect method.
        /// </summary>
        /// <param name="inst">InstitutionVM type element.</param>
        /// <returns>Task type element.</returns>
        public async Task<NewApiResult> Unselect(InstitutionVM inst)
        {
            NewApiResult newResult = new NewApiResult() { OperationResult = false, ActionName = "Unselect", Count = 0 };
            if (inst != null)
            {
                string json = await this.client.GetStringAsync(this.url + "Unselect/" + inst.InstitutionId.ToString());
                var list = JsonConvert.DeserializeObject<NewApiResult>(json);
                newResult.OperationResult = list.OperationResult;
                newResult.Count = list.Count;
            }

            return newResult;
        }
    }
}
