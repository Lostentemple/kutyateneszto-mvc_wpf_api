﻿// <copyright file="UserNamesWithIdSum.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class for the method UserNamesWithIdSum.
    /// </summary>
    public class UserNamesWithIdSum
    {
        /// <summary>
        /// Gets or sets name property of UserNamesWithIdSum.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets sum property of UserNamesWithIdSum.
        /// </summary>
        public int Sum { get; set; }

        /// <summary>
        /// Override for Equals for the testing.
        /// </summary>
        /// <param name="obj">object type element.</param>
        /// <returns>bool type element.</returns>
        public override bool Equals(object obj)
        {
            return obj is UserNamesWithIdSum sum &&
                   this.Name == sum.Name &&
                   this.Sum == sum.Sum;
        }

        /// <summary>
        /// Override for GetHashCode for the testing.
        /// </summary>
        /// <returns>int type element.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.Name, this.Sum);
        }
    }
}
