﻿// <copyright file="IManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Interface of ManagementLogic.
    /// </summary>
    public interface IManagementLogic
    {
        /// <summary>
        /// Deletes a Dog.
        /// </summary>
        /// <param name="id">int type element.</param>
        void DeleteDog(int id);

        /// <summary>
        /// Deletes an Insitution.
        /// </summary>
        /// <param name="id">int type element.</param>
        void DeleteInstitution(int id);

        /// <summary>
        /// Deletes a User.
        /// </summary>
        /// <param name="id">int type element.</param>
        void DeleteUser(int id);

        /// <summary>
        /// Inserts a Dog.
        /// </summary>
        /// <param name="dog">Dog type element.</param>
        void InsertDog(Dog dog);

        /// <summary>
        /// Inserts an institution.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        void InsertInstitution(Institution institution);

        /// <summary>
        /// Inserts a User.
        /// </summary>
        /// <param name="user">User type element.</param>
        void InsertUser(User user);

        /// <summary>
        /// Updates a Dog.
        /// </summary>
        /// <param name="dog">Dog type element.</param>
        /// <param name="id">int type element.</param>
        void UpdateDog(Dog dog, int id);

        /// <summary>
        /// Updates an Insitution.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        /// <param name="id">int type element.</param>
        void UpdateInstitution(Institution institution, int id);

        /// <summary>
        /// Updates a User.
        /// </summary>
        /// <param name="user">User type element.</param>
        /// <param name="id">int type element.</param>
        void UpdateUser(User user, int id);
    }
}