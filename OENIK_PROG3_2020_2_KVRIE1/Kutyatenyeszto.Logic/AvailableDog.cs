﻿// <copyright file="AvailableDog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class for the method UserNamesWithIdSum.
    /// </summary>
    public class AvailableDog
    {
        /// <summary>
        /// Gets or sets a value indicating whether isAvailable property of AvailableDog.
        /// </summary>
        public bool IsAvailable { get; set; }

        /// <summary>
        /// Gets or sets count property of AvailableDog.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// Override for Equals for the testing.
        /// </summary>
        /// <param name="obj">object type element.</param>
        /// <returns>bool type element.</returns>
        public override bool Equals(object obj)
        {
            return obj is AvailableDog dog &&
                   this.IsAvailable == dog.IsAvailable &&
                   this.Count == dog.Count;
        }

        /// <summary>
        /// Override for GetHashCode for the testing.
        /// </summary>
        /// <returns>int type element.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.IsAvailable, this.Count);
        }
    }
}
