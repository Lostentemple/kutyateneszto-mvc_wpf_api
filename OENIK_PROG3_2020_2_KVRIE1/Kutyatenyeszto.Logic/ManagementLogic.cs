// <copyright file="ManagementLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using System.Collections.Generic;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Repository;

    /// <summary>
    /// Class to manage repositories.
    /// </summary>
    public class ManagementLogic : IManagementLogic
    {
        private readonly IWorkWithDatabase dbcontext;

        /// <summary>
        /// Initializes a new instance of the <see cref="ManagementLogic"/> class.
        /// Connection between database and ManagementLogic.
        /// </summary>
        /// <param name="dbcontext">KutyatenyesztoDbContext type element.</param>
        public ManagementLogic(IWorkWithDatabase dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        /// <summary>
        /// Inserts a new Dog.
        /// </summary>
        /// <param name="dog">Dog type element.</param>
        public void InsertDog(Dog dog)
        {
            this.dbcontext.Dogs.Insert(dog);
        }

        /// <summary>
        /// Inserts a new User.
        /// </summary>
        /// <param name="user">User type element.</param>
        public void InsertUser(User user)
        {
            this.dbcontext.Users.Insert(user);
        }

        /// <summary>
        /// Inserts a new Institution.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        public void InsertInstitution(Institution institution)
        {
            this.dbcontext.Institutions.Insert(institution);
        }

        /// <summary>
        /// Updates an existing Dog.
        /// </summary>
        /// <param name="dog">Dog type element.</param>
        /// <param name="id">int type element.</param>
        public void UpdateDog(Dog dog, int id)
        {
            this.dbcontext.Dogs.Update(dog);
        }

        /// <summary>
        /// Updates an existing User.
        /// </summary>
        /// <param name="user">User type element.</param>
        /// <param name="id">int type element.</param>
        public void UpdateUser(User user, int id)
        {
            this.dbcontext.Users.Update(user, id);
        }

        /// <summary>
        /// Updates an existing Institution.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        /// <param name="id">int type element.</param>
        public void UpdateInstitution(Institution institution, int id)
        {
            this.dbcontext.Institutions.Update(institution, id);
        }

        /// <summary>
        /// Deletes a Dog.
        /// </summary>
        /// <param name="id">int type element.</param>
        public void DeleteDog(int id)
        {
            this.dbcontext.Dogs.Delete(id);
        }

        /// <summary>
        /// Deletes a User.
        /// </summary>
        /// <param name="id">int type element.</param>
        public void DeleteUser(int id)
        {
            this.dbcontext.Users.Delete(id);
        }

        /// <summary>
        /// Deletes an Institution.
        /// </summary>
        /// <param name="id">int type element.</param>
        public void DeleteInstitution(int id)
        {
            this.dbcontext.Institutions.Delete(id);
        }
    }
}