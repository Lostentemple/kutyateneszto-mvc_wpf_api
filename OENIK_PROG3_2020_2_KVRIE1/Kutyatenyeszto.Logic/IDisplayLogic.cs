﻿// <copyright file="IDisplayLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using System.Collections.Generic;
    using Kutyatenyeszto.Data.Models;

    /// <summary>
    /// Interface of DisplayLogic.
    /// </summary>
    public interface IDisplayLogic
    {
        /// <summary>
        /// Returns all Dogs.
        /// </summary>
        /// <returns>IList type element.</returns>
        IList<Dog> GetAllDogs();

        /// <summary>
        /// Returns all Institutions.
        /// </summary>
        /// <returns>IList type element.</returns>
        IList<Institution> GetAllInstitutions();

        /// <summary>
        /// Returns all Users.
        /// </summary>
        /// <returns>IList type element.</returns>
        IList<User> GetAllUsers();

        /// <summary>
        /// Returns a Dog.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Dog type element.</returns>
        Dog GetOneDog(int id);

        /// <summary>
        /// Returns an Institution.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Institution type element.</returns>
        Institution GetOneInstitution(int id);

        /// <summary>
        /// Returns a User.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>User type element.</returns>
        User GetOneUser(int id);
    }
}