﻿// <copyright file="AverageDogAge.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using System;
    using System.Collections.Generic;
    using System.Text;

    /// <summary>
    /// Class for the method AverageAgesOfDogs.
    /// </summary>
    public class AverageDogAge
    {
        /// <summary>
        /// Gets or sets dogBreed property of AverageDogAge.
        /// </summary>
        public string DogBreed { get; set; }

        /// <summary>
        /// Gets or sets avgAge property of AverageDogAge.
        /// </summary>
        public double AvgAge { get; set; }

        /// <summary>
        /// Override for Equals for the testing.
        /// </summary>
        /// <param name="obj">object type element.</param>
        /// <returns>bool type element.</returns>
        public override bool Equals(object obj)
        {
            return obj is AverageDogAge age &&
                   this.DogBreed == age.DogBreed &&
                   this.AvgAge == age.AvgAge;
        }

        /// <summary>
        /// Override for GetHashCode for the testing.
        /// </summary>
        /// <returns>int type element.</returns>
        public override int GetHashCode()
        {
            return HashCode.Combine(this.DogBreed, this.AvgAge);
        }
    }
}
