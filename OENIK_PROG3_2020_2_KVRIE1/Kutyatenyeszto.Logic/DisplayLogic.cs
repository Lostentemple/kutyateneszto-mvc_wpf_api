// <copyright file="DisplayLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic
{
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Runtime.CompilerServices;
    using System.Threading.Tasks;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Repository;

    /// <summary>
    /// Class to display elements from repositories and query results.
    /// </summary>
    public class DisplayLogic : IDisplayLogic
    {
        private readonly IWorkWithDatabase dbcontext;

        /// <summary>
        /// Initializes a new instance of the <see cref="DisplayLogic"/> class.
        /// Connection between database and DisplayLogic.
        /// </summary>
        /// <param name="dbcontext">KutyatenyesztoDbContext type element.</param>
        public DisplayLogic(IWorkWithDatabase dbcontext)
        {
            this.dbcontext = dbcontext;
        }

        /// <summary>
        /// Returns one Dog.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Dog type element.</returns>
        public Dog GetOneDog(int id)
        {
            return this.dbcontext.Dogs.GetById(id);
        }

        /// <summary>
        /// Returns one User.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>User type element.</returns>
        public User GetOneUser(int id)
        {
            return this.dbcontext.Users.GetById(id);
        }

        /// <summary>
        /// Returns one Institution.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>Institution type element.</returns>
        public Institution GetOneInstitution(int id)
        {
            return this.dbcontext.Institutions.GetById(id);
        }

        /// <summary>
        /// Returns all Dogs.
        /// </summary>
        /// <returns>IList type element.</returns>
        public IList<Dog> GetAllDogs()
        {
            return this.dbcontext.Dogs.GetAll().ToList();
        }

        /// <summary>
        /// Returns all Users.
        /// </summary>
        /// <returns>IList type element.</returns>
        public IList<User> GetAllUsers()
        {
            return this.dbcontext.Users.GetAll().ToList();
        }

        /// <summary>
        /// Returns all Institutions.
        /// </summary>
        /// <returns>IList type element.</returns>
        public IList<Institution> GetAllInstitutions()
        {
            return this.dbcontext.Institutions.GetAll().ToList();
        }

        /// <summary>
        /// Returns the average dog age by every breed.
        /// </summary>
        /// <returns>IList type element.</returns>
        public IList<AverageDogAge> AverageAgesOfDogs()
        {
            var avg = from dogs in this.GetAllDogs()
                let item = new { dogs.Breed, dogs.Age }
                group item by item.Breed
                into grp
                select new AverageDogAge()
                {
                    DogBreed = grp.Key,
                    AvgAge = grp.Average(dog => dog.Age),
                };
            return avg.ToList();
        }

        /// <summary>
        /// Returns the average dog age by every breed.
        /// </summary>
        /// <returns>IList type element.</returns>
        public IList<AvailableDog> AvailableDogs()
        {
            var available = from dogs in this.GetAllDogs()
                let item = new { IsAvailable = dogs.IsAccessible, dogs.Name }
                group item by item.IsAvailable
                into grp
                select new AvailableDog()
                {
                    IsAvailable = grp.Key,
                    Count = grp.Count(),
                };
            return available.ToList();
        }

        /// <summary>
        /// Returns each usernames and with the sum of their id.
        /// </summary>
        /// <returns>IList type element.</returns>
        public IList<UserNamesWithIdSum> UserNamesWithIdSum()
        {
            var usersWithIdSums = from users in this.GetAllUsers()
                let item = new { Name = users.UserName, Ids = users.UserId }
                group item by item.Name
                into grp
                select new UserNamesWithIdSum()
                {
                    Name = grp.Key,
                    Sum = grp.Sum(x => x.Ids),
                };
            return usersWithIdSums.ToList();
        }

        /// <summary>
        /// Async method of AverageAgesOfDogs.
        /// </summary>
        /// <returns>IList type element.</returns>
        public Task<IList<AverageDogAge>> AverageAgesOfDogsAsync()
        {
            return Task.Run(() => this.AverageAgesOfDogs());
        }

        /// <summary>
        /// Async method of AvailableDogs.
        /// </summary>
        /// <returns>IList type element.</returns>
        public Task<IList<AvailableDog>> AvailableDogsAsync()
        {
            return Task.Run(() => this.AvailableDogs());
        }

        /// <summary>
        /// Async method of UserNamesWithIdSumAsync.
        /// </summary>
        /// <returns>IList type element.</returns>
        public Task<IList<UserNamesWithIdSum>> UserNamesWithIdSumAsync()
        {
            return Task.Run(() => this.UserNamesWithIdSum());
        }
    }
}