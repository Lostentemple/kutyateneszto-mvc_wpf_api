﻿// <copyright file="DisplayLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the DisplayLogic methods.
    /// </summary>
    [TestFixture]
    public class DisplayLogicTest
    {
        /// <summary>
        /// GetOneDog's test method.
        /// </summary>
        [Test]
        public void GetOneDogTest()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            Dog dog = new Dog() { DogId = 1 };
            mockedWorkWithDatabase.Setup(x => x.Dogs.GetById(It.IsAny<int>())).Returns(dog);
            DisplayLogic test = new DisplayLogic(mockedWorkWithDatabase.Object);

            var result = test.GetOneDog(It.IsAny<int>());

            mockedWorkWithDatabase.Verify(x => x.Dogs.GetById(It.IsAny<int>()), Times.Once);
        }

        /// <summary>
        /// GetAllInstitutions's test method.
        /// </summary>
        [Test]
        public void GetAllInstitutionsTest()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            Institution institution1 = new Institution() { InstitutionId = 1 };
            Institution institution2 = new Institution() { InstitutionId = 2 };
            mockedWorkWithDatabase.Setup(x => x.Institutions.GetAll());
            DisplayLogic test = new DisplayLogic(mockedWorkWithDatabase.Object);

            var result = test.GetAllInstitutions();

            mockedWorkWithDatabase.Verify(x => x.Institutions.GetAll(), Times.Once);
        }

        /// <summary>
        /// AverageAgesOfDogs's test method.
        /// </summary>
        [Test]
        public void AverageAgesOfDogsTest()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            List<Dog> dogList = new List<Dog>()
            {
                new Dog() { Breed = "Vizsla", Age = 4 },
                new Dog() { Breed = "Vizsla", Age = 6 },
                new Dog() { Breed = "Puli", Age = 8 },
                new Dog() { Breed = "Puli", Age = 10 },
            };

            List<AverageDogAge> averageDogAge = new List<AverageDogAge>()
            {
                new AverageDogAge() { DogBreed = "Vizsla", AvgAge = 5 },
                new AverageDogAge() { DogBreed = "Puli", AvgAge = 9 },
            };

            mockedWorkWithDatabase.Setup(x => x.Dogs.GetAll()).Returns(dogList.AsQueryable);
            DisplayLogic test = new DisplayLogic(mockedWorkWithDatabase.Object);

            var result = test.AverageAgesOfDogs();

            Assert.That(result, Is.EquivalentTo(averageDogAge));
            mockedWorkWithDatabase.Verify(x => x.Dogs.GetAll(), Times.Once);
        }

        /// <summary>
        /// AvailableDogs's test method.
        /// </summary>
        [Test]
        public void AvailableDogsTest()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            List<Dog> dogList = new List<Dog>()
            {
                new Dog() { IsAccessible = true, Name = "Vilmost" },
                new Dog() { IsAccessible = true, Name = "Pisti" },
                new Dog() { IsAccessible = false, Name = "Bundás" },
            };

            List<AvailableDog> isAccessibleDogs = new List<AvailableDog>()
            {
                new AvailableDog() { IsAvailable = true, Count = 2 },
                new AvailableDog() { IsAvailable = false, Count = 1 },
            };

            mockedWorkWithDatabase.Setup(x => x.Dogs.GetAll()).Returns(dogList.AsQueryable);
            DisplayLogic test = new DisplayLogic(mockedWorkWithDatabase.Object);

            var result = test.AvailableDogs();

            Assert.That(result, Is.EquivalentTo(isAccessibleDogs));
            mockedWorkWithDatabase.Verify(x => x.Dogs.GetAll(), Times.Once);
        }

        /// <summary>
        /// UserNamesWithIdSum's test method.
        /// </summary>
        [Test]
        public void UserNamesWithIdSumTest()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            List<User> userList = new List<User>()
            {
                new User() { UserName = "Pistike", UserId = 1 },
                new User() { UserName = "Pistike", UserId = 2 },
                new User() { UserName = "Anna", UserId = 3 },
                new User() { UserName = "Anna", UserId = 4 },
                new User() { UserName = "Vilike", UserId = 5 },
            };

            List<UserNamesWithIdSum> expectedResult = new List<UserNamesWithIdSum>()
            {
                new UserNamesWithIdSum() { Name = "Pistike", Sum = 3 },
                new UserNamesWithIdSum() { Name = "Anna", Sum = 7 },
                new UserNamesWithIdSum() { Name = "Vilike", Sum = 5 },
            };

            mockedWorkWithDatabase.Setup(x => x.Users.GetAll()).Returns(userList.AsQueryable);
            DisplayLogic test = new DisplayLogic(mockedWorkWithDatabase.Object);

            var result = test.UserNamesWithIdSum();

            Assert.That(result, Is.EquivalentTo(expectedResult));
            mockedWorkWithDatabase.Verify(x => x.Users.GetAll(), Times.Once);
        }
    }
}
