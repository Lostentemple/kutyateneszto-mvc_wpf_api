﻿// <copyright file="ManagementLogicTest.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Logic.Tests
{
    using System;
    using System.Collections.Generic;
    using System.Text;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Repository;
    using Moq;
    using NUnit.Framework;

    /// <summary>
    /// Class to test the ManagementLogic methods.
    /// </summary>
    [TestFixture]
    public class ManagementLogicTest
    {
        /// <summary>
        /// InsertUser's test method.
        /// </summary>
        [Test]
        public void InsertUserTest()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            mockedWorkWithDatabase.Setup(x => x.Users.Insert(It.IsAny<User>()));
            ManagementLogic test = new ManagementLogic(mockedWorkWithDatabase.Object);
            test.InsertUser(It.IsAny<User>());
            mockedWorkWithDatabase.Verify(x => x.Users.Insert(It.IsAny<User>()), Times.Once);
        }

        /// <summary>
        /// UpdateInstitution's test method.
        /// </summary>
        [Test]
        public void UpdateInstitution()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            Institution institution = new Institution() { InstitutionId = 1 };
            mockedWorkWithDatabase.Setup(x => x.Institutions.Update(It.IsAny<Institution>(), institution.InstitutionId));
            ManagementLogic test = new ManagementLogic(mockedWorkWithDatabase.Object);
            test.UpdateInstitution(It.IsAny<Institution>(), institution.InstitutionId);
            mockedWorkWithDatabase.Verify(x => x.Institutions.Update(It.IsAny<Institution>(), institution.InstitutionId), Times.Once);
        }

        /// <summary>
        /// DeleteDog's test method.
        /// </summary>
        [Test]
        public void DeleteDog()
        {
            Mock<IWorkWithDatabase> mockedWorkWithDatabase = new Mock<IWorkWithDatabase>();
            Dog dog = new Dog() { DogId = 1 };
            mockedWorkWithDatabase.Setup(x => x.Dogs.Delete(dog.DogId));
            ManagementLogic test = new ManagementLogic(mockedWorkWithDatabase.Object);
            test.DeleteDog(dog.DogId);
            mockedWorkWithDatabase.Verify(x => x.Dogs.Delete(dog.DogId), Times.Once);
        }
    }
}
