﻿// <copyright file="MainViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using CommonServiceLocator;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;
    using Kutyatenyeszto.WPF.BL;
    using Kutyatenyeszto.WPF.Data;

    /// <summary>
    /// Class for the main view model.
    /// </summary>
    internal class MainViewModel : ViewModelBase
    {
        private IUserLogic logic;
        private UserModel userSelected;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        /// <param name="logic">IUserLogic type element.</param>
        public MainViewModel(IUserLogic logic)
        {
            this.logic = logic;

            this.Users = new ObservableCollection<UserModel>();

            if (!this.IsInDesignMode)
            {
                List<UserModel> list = this.logic.GetAllUsers().ToList();
                foreach (var item in list)
                {
                    this.Users.Add(item);
                }
            }
            else
            {
                UserModel p2 = new UserModel() { Name = "Wild Bill 2" };
                UserModel p3 = new UserModel() { Name = "Wild Bill 3", Gender = GenderTypes.Male };
                this.Users.Add(p2);
                this.Users.Add(p3);
            }

            this.AddCmd = new RelayCommand(() => this.logic.AddUser(this.Users));
            this.ModCmd = new RelayCommand(() => this.logic.ModUser(this.UserSelected));
            this.DelCmd = new RelayCommand(() => this.logic.DelUser(this.Users, this.UserSelected));
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="MainViewModel"/> class.
        /// </summary>
        public MainViewModel()
            : this(IsInDesignModeStatic ? null : ServiceLocator.Current.GetInstance<IUserLogic>())
        {
        }

        /// <summary>
        /// Gets or sets a user model element.
        /// </summary>
        public UserModel UserSelected
        {
            get { return this.userSelected; }
            set { this.Set(ref this.userSelected, value); }
        }

        /// <summary>
        /// Gets collection for user model elements.
        /// </summary>
        public ObservableCollection<UserModel> Users { get; private set; }

        /// <summary>
        /// Gets icommand type element for add.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets icommand type element for modification.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets icommand type element for delete.
        /// </summary>
        public ICommand DelCmd { get; private set; }
    }
}
