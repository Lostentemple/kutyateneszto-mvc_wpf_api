﻿// <copyright file="EditorViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.VM
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;
    using Kutyatenyeszto.WPF.Data;

    /// <summary>
    /// Class for edit view model.
    /// </summary>
    internal class EditorViewModel : ViewModelBase
    {
        private UserModel user;

        /// <summary>
        /// Initializes a new instance of the <see cref="EditorViewModel"/> class.
        /// </summary>
        public EditorViewModel()
        {
            this.user = new UserModel();
            if (this.IsInDesignMode)
            {
                this.user.Name = "Unknown Béla XXX";
                this.user.Password = "1234";
            }
        }

        /// <summary>
        /// Gets array for enum.
        /// </summary>
        public static Array Genders
        {
            get { return Enum.GetValues(typeof(GenderTypes)); }
        }

        /// <summary>
        /// Gets or sets Usermodel type element.
        /// </summary>
        public UserModel User
        {
            get { return this.user; }
            set { this.Set(ref this.user, value); }
        }
    }
}
