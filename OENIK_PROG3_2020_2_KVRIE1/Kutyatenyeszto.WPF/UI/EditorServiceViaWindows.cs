﻿// <copyright file="EditorServiceViaWindows.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.UI
{
    using Kutyatenyeszto.WPF.BL;
    using Kutyatenyeszto.WPF.Data;

    /// <summary>
    /// Class for edit user method.
    /// </summary>
    internal class EditorServiceViaWindows : IEditorService
    {
        /// <summary>
        /// Returns true or false based on given WPFUser type element.
        /// </summary>
        /// <param name="u">UserModel type element.</param>
        /// <returns>Bool type element.</returns>
        public bool EditUser(UserModel u)
        {
            EditorWindow win = new (u);
            return win.ShowDialog() ?? false;
        }
    }
}
