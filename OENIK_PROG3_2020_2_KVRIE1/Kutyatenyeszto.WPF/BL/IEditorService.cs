﻿// <copyright file="IEditorService.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Kutyatenyeszto.WPF.Data;

    /// <summary>
    /// Interface for editor service.
    /// </summary>
    internal interface IEditorService
    {
        /// <summary>
        /// Signature of EditUser method.
        /// </summary>
        /// <param name="u">WPFUser type element.</param>
        /// <returns>Bool type element.</returns>
        bool EditUser(UserModel u);
    }
}
