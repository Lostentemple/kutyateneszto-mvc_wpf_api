﻿// <copyright file="IUserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using Kutyatenyeszto.WPF.Data;

    /// <summary>
    /// Interface for UserLogic.
    /// </summary>
    internal interface IUserLogic
    {
        /// <summary>
        /// Inserting new user to database and viwdmodel.
        /// </summary>
        /// <param name="list">List type element.</param>
        void AddUser(IList<UserModel> list);

        /// <summary>
        /// Changing user in database and viwdmodel.
        /// </summary>
        /// <param name="playerToModify">WPFUser type element.</param>
        void ModUser(UserModel playerToModify);

        /// <summary>
        /// Deleting a user from database and viwdmodel.
        /// </summary>
        /// <param name="list">List type element.</param>
        /// <param name="user">WPFUser type element.</param>
        void DelUser(IList<UserModel> list, UserModel user);

        /// <summary>
        /// Listing all users from database.
        /// </summary>
        /// <returns>List type element.</returns>
        IList<UserModel> GetAllUsers();
    }
}
