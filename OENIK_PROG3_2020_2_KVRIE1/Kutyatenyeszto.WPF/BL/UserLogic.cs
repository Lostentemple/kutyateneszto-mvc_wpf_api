﻿// <copyright file="UserLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.BL
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Logic;
    using Kutyatenyeszto.Repository;
    using Kutyatenyeszto.WPF.Data;

    /// <summary>
    /// Class for user management.
    /// </summary>
    internal class UserLogic : IUserLogic
    {
        private readonly IDisplayLogic display;
        private readonly IManagementLogic management;
        private readonly IEditorService editorService;
        private readonly IMessenger messengerService;
        private int? highestId;

        /// <summary>
        /// Initializes a new instance of the <see cref="UserLogic"/> class.
        /// </summary>
        /// <param name="service">Ieditorservice type element.</param>
        /// <param name="messenger">Imessenger type element.</param>
        /// <param name="display">IDisplayLogic type element.</param>
        /// <param name="management">IManagementLogic type element.</param>
        public UserLogic(IEditorService service, IMessenger messenger, IDisplayLogic display, IManagementLogic management)
        {
            this.editorService = service;
            this.messengerService = messenger;
            this.display = display;
            this.management = management;
        }

        /// <summary>
        /// Method for adding new user to the database.
        /// </summary>
        /// <param name="list">Ilist type element.</param>
        public void AddUser(IList<UserModel> list)
        {
            UserModel newUser = new ();
            if (this.editorService.EditUser(newUser) == true)
            {
                if (this.highestId == null)
                {
                    this.highestId = this.display.GetAllUsers().ToList().Last().UserId;
                }

                User addNewUser = new ()
                {
                    UserName = newUser.Name,
                    Password = newUser.Password,
                    BirthDate = DateTime.Now,
                    Gender = (Kutyatenyeszto.Data.Models.GenderTypes)newUser.Gender,
                    PhoneNumber = newUser.PhoneNumber,
                    EmailAddress = newUser.Email,
                    LastLoginDate = null,
                };

                this.management.InsertUser(addNewUser);
                this.highestId++;
                newUser.Id = (int)this.highestId;
                list.Add(newUser);
                this.messengerService.Send("ADD OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("ADD CANCEL", "LogicResult");
            }
        }

        /// <summary>
        /// Deleting a user from the database.
        /// </summary>
        /// <param name="list">Ilist type element.</param>
        /// <param name="user">Usermodel type element.</param>
        public void DelUser(IList<UserModel> list, UserModel user)
        {
            if (user != null && list.Remove(user))
            {
                this.management.DeleteUser(user.Id);
                this.messengerService.Send("DELETE OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("DELETE FAILED", "LogicResult");
            }
        }

        /// <summary>
        /// Method to list all users.
        /// </summary>
        /// <returns>Ilist type element.</returns>
        public IList<UserModel> GetAllUsers()
        {
            List<User> userList = this.display.GetAllUsers().ToList();
            List<UserModel> wpfUserList = new ();
            foreach (var item in userList)
            {
                UserModel wpfUser = new ();
                wpfUser.Id = item.UserId;
                wpfUser.Name = item.UserName;
                wpfUser.Password = item.Password;
                wpfUser.Gender = (Data.GenderTypes)item.Gender;
                wpfUser.BirthDate = item.BirthDate;
                wpfUser.PhoneNumber = item.PhoneNumber;
                wpfUser.LastLoginDate = item.LastLoginDate;
                wpfUser.Email = item.EmailAddress;
                wpfUserList.Add(wpfUser);
            }

            return wpfUserList;
        }

        /// <summary>
        /// Method to modify users.
        /// </summary>
        /// <param name="userToModify">Usermodel type element.</param>
        public void ModUser(UserModel userToModify)
        {
            if (userToModify == null)
            {
                this.messengerService.Send("EDIT FAILED", "LogicResult");
                return;
            }

            UserModel clone = new ();
            clone.CopyFrom(userToModify);
            if (this.editorService.EditUser(clone) == true)
            {
                userToModify.CopyFrom(clone);
                User addNewUser = new ()
                {
                    UserName = userToModify.Name,
                    Password = userToModify.Password,
                    BirthDate = userToModify.BirthDate,
                    Gender = (Kutyatenyeszto.Data.Models.GenderTypes)userToModify.Gender,
                    PhoneNumber = userToModify.PhoneNumber,
                    EmailAddress = userToModify.Email,
                    LastLoginDate = null,
                };

                this.management.UpdateUser(addNewUser, userToModify.Id);
                this.messengerService.Send("EDIT OK", "LogicResult");
            }
            else
            {
                this.messengerService.Send("EDIT CANCEL", "LogicResult");
            }
        }
    }
}
