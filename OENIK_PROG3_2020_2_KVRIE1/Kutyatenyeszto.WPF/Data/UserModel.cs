﻿// <copyright file="UserModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WPF.Data
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// Enum for genders.
    /// </summary>
    public enum GenderTypes
    {
        /// <summary>
        /// Male enum item.
        /// </summary>
        Male,

        /// <summary>
        /// Female enum item.
        /// </summary>
        Female,
    }

    /// <summary>
    /// User class of UI.
    /// </summary>
    public class UserModel : ObservableObject
    {
        private int id;
        private string name;
        private string password;
        private GenderTypes gender;
        private DateTime birthdate = DateTime.Now.Date;
        private string email;
        private string phonenumber;
        private DateTime? lastLoginDate;

        /// <summary>
        /// Gets or sets the id of a User.
        /// </summary>
        public int Id
        {
            get { return this.id; }
            set { this.Set(ref this.id, value); }
        }

        /// <summary>
        /// Gets or sets the name of a User.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets the password of a User.
        /// </summary>
        public string Password
        {
            get { return this.password; }
            set { this.Set(ref this.password, value); }
        }

        /// <summary>
        /// Gets or sets the gender of a User.
        /// </summary>
        public GenderTypes Gender
        {
            get { return this.gender; }
            set { this.Set(ref this.gender, value); }
        }

        /// <summary>
        /// Gets or sets the birthday of a User.
        /// </summary>
        public DateTime BirthDate
        {
            get { return this.birthdate; }
            set { this.Set(ref this.birthdate, value); }
        }

        /// <summary>
        /// Gets or sets the email address of a User.
        /// </summary>
        public string Email
        {
            get { return this.email; }
            set { this.Set(ref this.email, value); }
        }

        /// <summary>
        /// Gets or sets the phone number of a User.
        /// </summary>
        public string PhoneNumber
        {
            get { return this.phonenumber; }
            set { this.Set(ref this.phonenumber, value); }
        }

        /// <summary>
        /// Gets or sets the last login date of a User.
        /// </summary>
        public DateTime? LastLoginDate
        {
            get { return this.lastLoginDate; }
            set { this.Set(ref this.lastLoginDate, value); }
        }

        /// <summary>
        /// Method to make a clone so the bindings can work properly.
        /// </summary>
        /// <param name="other">UserModel type element.</param>
        public void CopyFrom(UserModel other)
        {
            this.GetType().GetProperties().ToList().
                ForEach(property => property.SetValue(this, property.GetValue(other)));
        }
    }
}
