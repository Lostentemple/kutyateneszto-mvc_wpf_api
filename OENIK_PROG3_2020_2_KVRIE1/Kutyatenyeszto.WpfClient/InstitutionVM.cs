﻿// <copyright file="InstitutionVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight;

    /// <summary>
    /// InstitutionVM class.
    /// </summary>
    public class InstitutionVM : ObservableObject
    {
        private int institutionId;
        private string name;
        private string address;
        private string phoneNumber;
        private string director;
        private int capacity;

        /// <summary>
        /// Gets or sets institutionId property.
        /// </summary>
        public int InstitutionId
        {
            get { return this.institutionId; }
            set { this.Set(ref this.institutionId, value); }
        }

        /// <summary>
        /// Gets or sets name property.
        /// </summary>
        public string Name
        {
            get { return this.name; }
            set { this.Set(ref this.name, value); }
        }

        /// <summary>
        /// Gets or sets address property.
        /// </summary>
        public string Address
        {
            get { return this.address; }
            set { this.Set(ref this.address, value); }
        }

        /// <summary>
        /// Gets or sets phoneNumber property.
        /// </summary>
        public string PhoneNumber
        {
            get { return this.phoneNumber; }
            set { this.Set(ref this.phoneNumber, value); }
        }

        /// <summary>
        /// Gets or sets director property.
        /// </summary>
        public string Director
        {
            get { return this.director; }
            set { this.Set(ref this.director, value); }
        }

        /// <summary>
        /// Gets or sets capacity property.
        /// </summary>
        public int Capacity
        {
            get { return this.capacity; }
            set { this.Set(ref this.capacity, value); }
        }

        /// <summary>
        /// Copyfrom method.
        /// </summary>
        /// <param name="other">InstitutionVM type element.</param>
        public void CopyFrom(InstitutionVM other)
        {
            if (other == null)
            {
                return;
            }

            this.institutionId = other.institutionId;
            this.name = other.name;
            this.address = other.address;
            this.phoneNumber = other.phoneNumber;
            this.director = other.director;
            this.capacity = other.capacity;
        }
    }
}
