﻿// <copyright file="MainVM.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Collections.ObjectModel;
    using System.Linq;
    using System.Text;
    using System.Threading.Tasks;
    using System.Windows.Input;
    using GalaSoft.MvvmLight;
    using GalaSoft.MvvmLight.Command;

    /// <summary>
    /// MainVM class.
    /// </summary>
    internal class MainVM : ViewModelBase
    {
        private MainLogic logic;
        private InstitutionVM selectedInstitution;
        private ObservableCollection<InstitutionVM> allInstitutions;

        /// <summary>
        /// Initializes a new instance of the <see cref="MainVM"/> class.
        /// </summary>
        public MainVM()
        {
            this.logic = new MainLogic();

            this.LoadCmd = new RelayCommand(() =>
                this.AllInstitutions = new ObservableCollection<InstitutionVM>(this.logic.ApiGetInstitutions()));
            this.DelCmd = new RelayCommand(() => this.logic.ApiDelInstitution(this.selectedInstitution));
            this.AddCmd = new RelayCommand(() => this.logic.EditInsitution(null, this.EditorFunc));
            this.ModCmd = new RelayCommand(() => this.logic.EditInsitution(this.selectedInstitution, this.EditorFunc));
        }

        /// <summary>
        /// Gets or sets allInstitutions property.
        /// </summary>
        public ObservableCollection<InstitutionVM> AllInstitutions
        {
            get { return this.allInstitutions; }
            set { this.Set(ref this.allInstitutions, value); }
        }

        /// <summary>
        /// Gets or sets selectedInstitution property.
        /// </summary>
        public InstitutionVM SelectedInstitution
        {
            get { return this.selectedInstitution; }
            set { this.Set(ref this.selectedInstitution, value); }
        }

        /// <summary>
        /// Gets or sets editorFunc property.
        /// </summary>
        public Func<InstitutionVM, bool> EditorFunc { get; set; }

        /// <summary>
        /// Gets addCmd property.
        /// </summary>
        public ICommand AddCmd { get; private set; }

        /// <summary>
        /// Gets delCmd property.
        /// </summary>
        public ICommand DelCmd { get; private set; }

        /// <summary>
        /// Gets modCmd property.
        /// </summary>
        public ICommand ModCmd { get; private set; }

        /// <summary>
        /// Gets loadCmd property.
        /// </summary>
        public ICommand LoadCmd { get; private set; }
    }
}
