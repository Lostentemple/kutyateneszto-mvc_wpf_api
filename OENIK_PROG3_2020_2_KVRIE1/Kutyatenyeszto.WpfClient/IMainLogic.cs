﻿// <copyright file="IMainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WpfClient
{
    using System;
    using System.Collections.Generic;

    /// <summary>
    /// IMainLogic interface.
    /// </summary>
    internal interface IMainLogic
    {
        /// <summary>
        /// ApiDelInstitution method.
        /// </summary>
        /// <param name="institution">InstitutionVM type element.</param>
        void ApiDelInstitution(InstitutionVM institution);

        /// <summary>
        /// ApiGetInstitutions method.
        /// </summary>
        /// <returns>List type element.</returns>
        List<InstitutionVM> ApiGetInstitutions();

        /// <summary>
        /// EditInsitution method.
        /// </summary>
        /// <param name="institution">InstitutionVM type element.</param>
        /// <param name="editorFunc">Func type element.</param>
        void EditInsitution(InstitutionVM institution, Func<InstitutionVM, bool> editorFunc);
    }
}