﻿// <copyright file="MainLogic.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WpfClient
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Net.Http;
    using System.Text;
    using System.Text.Json;
    using System.Threading.Tasks;
    using GalaSoft.MvvmLight.Messaging;

    /// <summary>
    /// MainLogic class.
    /// </summary>
    internal class MainLogic : IMainLogic
    {
        private readonly string url = "https://localhost:44372/InstitutionsApi/";
        private readonly HttpClient client = new HttpClient();
        private readonly JsonSerializerOptions jsonOptions = new JsonSerializerOptions(JsonSerializerDefaults.Web);

        /// <summary>
        /// ApiGetInstitutions method.
        /// </summary>
        /// <returns>List type element.</returns>
        public List<InstitutionVM> ApiGetInstitutions()
        {
            var json = this.client.GetStringAsync(this.url + "all").Result;
            var list = JsonSerializer.Deserialize<List<InstitutionVM>>(json, this.jsonOptions);
            return list;
        }

        /// <summary>
        /// ApiDelInstitution method.
        /// </summary>
        /// <param name="institution">InstitutionVM type element.</param>
        public void ApiDelInstitution(InstitutionVM institution)
        {
            bool success = false;
            if (institution != null)
            {
                string json = this.client.GetStringAsync(this.url + "del/" + institution.InstitutionId.ToString()).Result;
                JsonDocument doc = JsonDocument.Parse(json);
                success = doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
            }

            this.SendMessage(success);
        }

        /// <summary>
        /// EditInsitution method.
        /// </summary>
        /// <param name="institution">InstitutionVM type element.</param>
        /// <param name="editorFunc">Func type element.</param>
        public void EditInsitution(InstitutionVM institution, Func<InstitutionVM, bool> editorFunc)
        {
            InstitutionVM clone = new InstitutionVM();
            if (institution != null)
            {
                clone.CopyFrom(institution);
            }

            bool? success = editorFunc?.Invoke(clone);
            if (success == true)
            {
                if (institution != null)
                {
                    success = this.ApiEditInstitution(clone, true);
                }
                else
                {
                    success = this.ApiEditInstitution(clone, false);
                }
            }

            this.SendMessage(success == true);
        }

        /// <summary>
        /// SendMessage method.
        /// </summary>
        /// <param name="success">bool type element.</param>
        private void SendMessage(bool success)
        {
            string msg = success ? "Operation completed successfully" : "Operation failed";
            Messenger.Default.Send(msg, "InstitutionResult");
        }

        /// <summary>
        /// ApiEditInstitution method.
        /// </summary>
        /// <param name="institution">InstitutionVM type element.</param>
        /// <param name="isEditing">bool type element.</param>
        /// <returns>bool type elements.</returns>
        private bool ApiEditInstitution(InstitutionVM institution, bool isEditing)
        {
            if (institution == null)
            {
                return false;
            }

            string myUrl = this.url + (isEditing ? "mod" : "add");
            Dictionary<string, string> postData = new Dictionary<string, string>();
            if (isEditing)
            {
                postData.Add("InstitutionId", institution.InstitutionId.ToString());
            }

            postData.Add("Name", institution.Name);
            postData.Add("Address", institution.Address);
            postData.Add("PhoneNumber", institution.PhoneNumber);
            postData.Add("Director", institution.Director);
            postData.Add("Capacity", institution.Capacity.ToString());

            string json = this.client.PostAsync(myUrl, new FormUrlEncodedContent(postData)).
                Result.Content.ReadAsStringAsync().Result;
            JsonDocument doc = JsonDocument.Parse(json);
            return doc.RootElement.EnumerateObject().First().Value.GetRawText() == "true";
        }
    }
}
