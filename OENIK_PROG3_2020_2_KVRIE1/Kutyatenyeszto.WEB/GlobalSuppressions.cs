﻿// <copyright file="GlobalSuppressions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

using System.Diagnostics.CodeAnalysis;

[assembly: SuppressMessage("Design", "CA1062:Validate arguments of public methods", Justification = "warning that makes no sense", Scope = "member", Target = "~M:Kutyatenyeszto.WEB.Controllers.InstitutionsController.#ctor(Kutyatenyeszto.Logic.IManagementLogic,Kutyatenyeszto.Logic.IDisplayLogic,AutoMapper.IMapper)")]
[assembly: SuppressMessage("Design", "CA1002:Do not expose generic lists", Justification = "warning that makes no sense", Scope = "member", Target = "~P:Kutyatenyeszto.WEB.Models.InstitutionListViewModel.ListOfInsitutions")]
[assembly: SuppressMessage("Usage", "CA2227:Collection properties should be read only", Justification = "warning that makes no sense", Scope = "member", Target = "~P:Kutyatenyeszto.WEB.Models.InstitutionListViewModel.ListOfInsitutions")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "warning that makes no sense", Scope = "type", Target = "~T:Kutyatenyeszto.WEB.Models.MapperFactory")]
[assembly: SuppressMessage("Design", "CA1052:Static holder types should be Static or NotInheritable", Justification = "warning that makes no sense", Scope = "type", Target = "~T:Kutyatenyeszto.WEB.Program")]
[assembly: SuppressMessage("Design", "CA1062", Justification = "Done", Scope = "")]
[assembly: SuppressMessage("", "CA5394", Justification = "done", Scope = "module")]
