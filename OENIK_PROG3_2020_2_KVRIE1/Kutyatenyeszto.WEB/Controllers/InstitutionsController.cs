﻿// <copyright file="InstitutionsController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Kutyatenyeszto.Logic;
    using Kutyatenyeszto.WEB.Models;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// Controller class.
    /// </summary>
    public class InstitutionsController : Controller
    {
        private readonly IManagementLogic manage;
        private readonly IDisplayLogic display;
        private readonly IMapper mapper;
        private readonly InstitutionListViewModel vm;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionsController"/> class.
        /// </summary>
        /// <param name="manage">IManagementLogic type element.</param>
        /// <param name="display">IDisplayLogic type element.</param>
        /// <param name="mapper">IMapper type element.</param>
        public InstitutionsController(IManagementLogic manage, IDisplayLogic display, IMapper mapper)
        {
            this.manage = manage;
            this.display = display;
            this.mapper = mapper;

            this.vm = new InstitutionListViewModel
            {
                EditedInstitution = new Models.Institution(),
            };

            var institutions = display.GetAllInstitutions();
            this.vm.ListOfInsitutions = mapper.Map<IList<Data.Models.Institution>, List<WEB.Models.Institution>>(institutions);
        }

        /// <summary>
        /// Index method.
        /// </summary>
        /// <returns>IActionResult type element.</returns>
        public IActionResult Index()
        {
            this.ViewData["editAction"] = "AddNew";
            return this.View("InstitutionIndex", this.vm);
        }

        /// <summary>
        /// Details method.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>IActionResult type element.</returns>
        public IActionResult Details(int id)
        {
            return this.View("InstitutionDetails", this.GetInstitutionModel(id));
        }

        /// <summary>
        /// Remove method.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>IActionResult type element.</returns>
        public IActionResult Remove(int id)
        {
            this.manage.DeleteInstitution(id);
            this.TempData["editResult"] = "Delete OK";
            return this.RedirectToAction(nameof(this.Index));
        }

        /// <summary>
        /// Remove method.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>IActionResult type element.</returns>
        public IActionResult Edit(int id)
        {
            this.ViewData["editAction"] = "Edit";
            this.vm.EditedInstitution = this.GetInstitutionModel(id);
            return this.View("InstitutionIndex", this.vm);
        }

        /// <summary>
        /// Edit method.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        /// <param name="editAction">string type element.</param>
        /// <returns>IActionResult type element.</returns>
        [HttpPost]
        public IActionResult Edit(Models.Institution institution, string editAction)
        {
            if (this.ModelState.IsValid && institution != null)
            {
                this.TempData["editResult"] = "Edit OK";
                if (editAction == "AddNew")
                {
                    Data.Models.Institution insert = new ()
                    {
                        Name = institution.Name,
                        Address = institution.Address,
                        PhoneNumber = institution.PhoneNumber,
                        Director = institution.Director,
                        Capacity = institution.Capacity,
                    };
                    this.manage.InsertInstitution(insert);
                }
                else
                {
                    Data.Models.Institution insert = new ()
                    {
                        InstitutionId = institution.InstitutionId,
                        Name = institution.Name,
                        Address = institution.Address,
                        PhoneNumber = institution.PhoneNumber,
                        Director = institution.Director,
                        Capacity = institution.Capacity,
                    };
                    this.manage.UpdateInstitution(insert, insert.InstitutionId);
                }

                return this.RedirectToAction(nameof(this.Index));
            }
            else
            {
                this.ViewData["editAction"] = "Edit";
                this.vm.EditedInstitution = institution;
                return this.View("InstitutionIndex", this.vm);
            }
        }

        private Models.Institution GetInstitutionModel(int id)
        {
            Data.Models.Institution oneInstitution = this.display.GetOneInstitution(id);
            return this.mapper.Map<Data.Models.Institution, Models.Institution>(oneInstitution);
        }
    }
}
