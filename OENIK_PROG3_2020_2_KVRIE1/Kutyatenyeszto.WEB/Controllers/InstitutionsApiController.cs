﻿// <copyright file="InstitutionsApiController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;
    using Kutyatenyeszto.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// InstitutionsApiController class.
    /// </summary>
    public class InstitutionsApiController : Controller
    {
        /// <summary>
        /// IManagementLogic property.
        /// </summary>
        private IManagementLogic manage;

        /// <summary>
        /// IDisplayLogic property.
        /// </summary>
        private IDisplayLogic display;

        /// <summary>
        /// IMapper property.
        /// </summary>
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="InstitutionsApiController"/> class.
        /// </summary>
        /// <param name="manage">IManagementLogic type element.</param>
        /// <param name="display">IDisplayLogic type element.</param>
        /// <param name="mapper">IMapper type element.</param>
        public InstitutionsApiController(IManagementLogic manage, IDisplayLogic display, IMapper mapper)
        {
            this.manage = manage;
            this.display = display;
            this.mapper = mapper;
        }

        /// <summary>
        /// Getall method.
        /// </summary>
        /// <returns>IEnumerable type element.</returns>
        [HttpGet]
        [ActionName("all")]
        public IEnumerable<Models.Institution> GetAll()
        {
            var institutions = this.display.GetAllInstitutions();
            return this.mapper.Map<IList<Data.Models.Institution>, List<Models.Institution>>(institutions);
        }

        /// <summary>
        /// DelOneInstitution method.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>ApiResult type element.</returns>
        [HttpGet]
        [ActionName("del")]
        public ApiResult DelOneInstitution(int id)
        {
            this.manage.DeleteInstitution(id);
            return new ApiResult() { OperationResult = true };
        }

        /// <summary>
        /// AddOneInstitution method.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        /// <returns>ApiResult type element.</returns>
        [HttpPost]
        [ActionName("add")]
        public ApiResult AddOneInstitution(Models.Institution institution)
        {
            bool success = true;
            try
            {
                Data.Models.Institution insert = new ()
                {
                    Name = institution.Name,
                    Address = institution.Address,
                    PhoneNumber = institution.PhoneNumber,
                    Director = institution.Director,
                    Capacity = institution.Capacity,
                };
                this.manage.InsertInstitution(insert);
            }
            catch (ArgumentException ex)
            {
                success = false;
                Console.WriteLine($"{ex}");
            }

            return new ApiResult() { OperationResult = success };
        }

        /// <summary>
        /// ModOneInsitution method.
        /// </summary>
        /// <param name="institution">Institution type element.</param>
        /// <returns>ApiResult type element.</returns>
        [HttpPost]
        [ActionName("mod")]
        public ApiResult ModOneInsitution(Models.Institution institution)
        {
            Data.Models.Institution insert = new ()
            {
                InstitutionId = institution.InstitutionId,
                Name = institution.Name,
                Address = institution.Address,
                PhoneNumber = institution.PhoneNumber,
                Director = institution.Director,
                Capacity = institution.Capacity,
            };
            this.manage.UpdateInstitution(insert, insert.InstitutionId);
            return new ApiResult() { OperationResult = true };
        }
    }
}
