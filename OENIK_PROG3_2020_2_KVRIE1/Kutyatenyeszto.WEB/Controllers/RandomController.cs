﻿// <copyright file="RandomController.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Security.Cryptography;
    using System.Threading.Tasks;
    using AutoMapper;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Logic;
    using Microsoft.AspNetCore.Mvc;

    /// <summary>
    /// RandomController class.
    /// </summary>
    public class RandomController : Controller
    {
        private const string Letters = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        private const string Numbers = "0123456789";

        /// <summary>
        /// IManagementLogic property.
        /// </summary>
        private IManagementLogic manage;

        /// <summary>
        /// IDisplayLogic property.
        /// </summary>
        private IDisplayLogic display;

        /// <summary>
        /// IMapper property.
        /// </summary>
        private IMapper mapper;

        /// <summary>
        /// Initializes a new instance of the <see cref="RandomController"/> class.
        /// </summary>
        /// <param name="manage">IManagementLogic type element.</param>
        /// <param name="display">IDisplayLogic type element.</param>
        /// <param name="mapper">IMapper type element.</param>
        public RandomController(IManagementLogic manage, IDisplayLogic display, IMapper mapper)
        {
            this.manage = manage;
            this.display = display;
            this.mapper = mapper;
        }

        /// <summary>
        /// Getall method.
        /// </summary>
        /// <returns>IEnumerable type element.</returns>
        [HttpGet]
        [ActionName("GetOne")]
        public Data.Models.Institution GetOne()
        {
            Random rnd = new Random();
            var inst = new Data.Models.Institution()
            {
                // InstitutionId = this.display.GetAllInstitutions().Max(x => x.InstitutionId) + 1,
                Name = new string(Enumerable.Repeat(Letters, 8).Select(x => x[RandomNumberGenerator.GetInt32(x.Length)]).ToArray()),
                Address = new string(Enumerable.Repeat(Letters, 10).Select(x => x[RandomNumberGenerator.GetInt32(x.Length)]).ToArray()),
                PhoneNumber = new string(Enumerable.Repeat(Numbers, 7).Select(x => x[RandomNumberGenerator.GetInt32(x.Length)]).ToArray()),
                Director = new string(Enumerable.Repeat(Letters, 5).Select(x => x[RandomNumberGenerator.GetInt32(x.Length)]).ToArray()),
                Capacity = rnd.Next(10, 30),
                Selected = false,
            };

            this.manage.InsertInstitution(inst);
            return inst;
        }

        /// <summary>
        /// Select method.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>ApiResult type element.</returns>
        [HttpGet]
        [ActionName("Select")]
        public NewApiResult Select(int id)
        {
            var institution = this.display.GetOneInstitution(id);

            Data.Models.Institution insert = new ()
            {
                InstitutionId = institution.InstitutionId,
                Name = institution.Name,
                Address = institution.Address,
                PhoneNumber = institution.PhoneNumber,
                Director = institution.Director,
                Capacity = institution.Capacity,
                Selected = true,
            };
            this.manage.UpdateInstitution(insert, insert.InstitutionId);
            return new NewApiResult(this.display) { OperationResult = true };
        }

        /// <summary>
        /// Unselect method.
        /// </summary>
        /// <param name="id">int type element.</param>
        /// <returns>ApiResult type element.</returns>
        [HttpGet]
        [ActionName("Unselect")]
        public NewApiResult Unselect(int id)
        {
            var institution = this.display.GetOneInstitution(id);

            Data.Models.Institution insert = new ()
            {
                InstitutionId = institution.InstitutionId,
                Name = institution.Name,
                Address = institution.Address,
                PhoneNumber = institution.PhoneNumber,
                Director = institution.Director,
                Capacity = institution.Capacity,
                Selected = false,
            };
            this.manage.UpdateInstitution(insert, insert.InstitutionId);
            return new NewApiResult(this.display) { OperationResult = true };
        }

        /// <summary>
        /// Selection method.
        /// </summary>
        /// <returns>IActionResult type element.</returns>
        [HttpGet]
        [ActionName("Selections")]
        public IActionResult Selection()
        {
            var model = this.display.GetAllInstitutions();
            return this.View("SelectionResult", model);
        }
    }
}
