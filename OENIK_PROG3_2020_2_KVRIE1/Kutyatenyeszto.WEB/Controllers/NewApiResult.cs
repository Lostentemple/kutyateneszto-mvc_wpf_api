﻿// <copyright file="NewApiResult.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Kutyatenyeszto.Logic;

    /// <summary>
    /// NewApiResult class.
    /// </summary>
    public class NewApiResult
    {
        private readonly IDisplayLogic display;

        /// <summary>
        /// Initializes a new instance of the <see cref="NewApiResult"/> class.
        /// </summary>
        /// <param name="display">IDisplayLogic type element.</param>
        public NewApiResult(IDisplayLogic display)
        {
            this.display = display;
        }

        /// <summary>
        /// Gets or sets a value indicating whether operationResult property.
        /// </summary>
        public bool OperationResult { get; set; }

        /// <summary>
        /// Gets selectedcount property.
        /// </summary>
        public int SelectedCount
        {
            get { return this.display.GetAllInstitutions().Where(x => x.Selected == true).Count(); }
        }

        /// <summary>
        /// Gets unselectedcount property.
        /// </summary>
        public int UnSelectedCount
        {
            get { return this.display.GetAllInstitutions().Where(x => x.Selected == false).Count(); }
        }
    }
}
