﻿// <copyright file="MapperFactory.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using AutoMapper;

    /// <summary>
    /// Class for mapping models.
    /// </summary>
    public class MapperFactory
    {
        /// <summary>
        /// CreateMapper method.
        /// </summary>
        /// <returns>IMapper type element.</returns>
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg =>
            {
                cfg.CreateMap<Data.Models.Institution, WEB.Models.Institution>().
                    ForMember(dest => dest.InstitutionId, map => map.MapFrom(src => src.InstitutionId)).
                    ForMember(dest => dest.Name, map => map.MapFrom(src => src.Name)).
                    ForMember(dest => dest.Address, map => map.MapFrom(src => src.Address)).
                    ForMember(dest => dest.PhoneNumber, map => map.MapFrom(src => src.PhoneNumber)).
                    ForMember(dest => dest.Director, map => map.MapFrom(src => src.Director)).
                    ForMember(dest => dest.Capacity, map => map.MapFrom(src => src.Capacity));
            });
            return config.CreateMapper();
        }
    }
}
