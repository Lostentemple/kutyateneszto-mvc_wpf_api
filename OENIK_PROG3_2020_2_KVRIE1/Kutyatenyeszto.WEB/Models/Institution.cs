﻿// <copyright file="Institution.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// Institution form model and insitution MVC viewmodel.
    /// </summary>
    public class Institution
    {
        /// <summary>
        /// Gets or sets primary key to identify users.
        /// </summary>
        [Display(Name = "Institution Id")]
        [Required]
        public int InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets name for the institution.
        /// </summary>
        [Display(Name = "Institution Name")]
        [Required]
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets address for the institution.
        /// </summary>
        [Display(Name = "Institution Address")]
        [Required]
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets phone number for the institution.
        /// </summary>
        [Display(Name = "Institution Phone number")]
        [Required]
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets director for the institution.
        /// </summary>
        [Display(Name = "Institution Director")]
        [Required]
        public string Director { get; set; }

        /// <summary>
        /// Gets or sets capacity for the institution.
        /// </summary>
        [Display(Name = "Institution Capacity")]
        [Required]
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets capacity for the institution.
        /// </summary>
        [Display(Name = "Institution Selected")]
        public string Selected { get; set; }
    }
}
