﻿// <copyright file="InstitutionListViewModel.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.WEB.Models
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;

    /// <summary>
    /// InstitutionListViewModel class.
    /// </summary>
    public class InstitutionListViewModel
    {
        /// <summary>
        /// Gets or sets listOfInsitutions.
        /// </summary>
        public List<Institution> ListOfInsitutions { get; set; }

        /// <summary>
        /// Gets or sets editedInstitution.
        /// </summary>
        public Institution EditedInstitution { get; set; }
    }
}
