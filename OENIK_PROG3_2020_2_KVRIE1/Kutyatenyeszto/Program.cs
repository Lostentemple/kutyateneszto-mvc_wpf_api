﻿// <copyright file="Program.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto
{
    using System;
    using System.Collections.Generic;
    using System.Globalization;
    using System.Linq;
    using ConsoleTools;
    using Kutyatenyeszto.Data;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Logic;
    using Kutyatenyeszto.MenuActions;
    using Kutyatenyeszto.Repository;
    using static System.Console;

    /// <summary>
    /// program name.
    /// </summary>
    public static class Program
    {
        private static UserActionDog userActionDog;
        private static UserActionUser userActionUser;
        private static UserActionInstitution userActionInstitution;

        /// <summary>
        /// Main method.
        /// </summary>
        private static void Main()
        {
            var context = new KutyatenyesztoDbContext();
            IWorkWithDatabase dogBreeder = new WorkwithDatabase(context);
            var displayLogic = new DisplayLogic(dogBreeder);
            var managementLogic = new ManagementLogic(dogBreeder);
            userActionDog = new UserActionDog(dogBreeder, displayLogic, managementLogic);
            userActionUser = new UserActionUser(dogBreeder, displayLogic, managementLogic);
            userActionInstitution = new UserActionInstitution(dogBreeder, displayLogic, managementLogic);
            var menu = new ConsoleMenu().Add("List all data", () =>
                {
                    WriteLine("Dogs: ");
                    foreach (var item in displayLogic.GetAllDogs())
                    {
                        WriteLine($"Id: {item.DogId} Name: {item.Name} Breed: {item.Breed}");
                    }

                    WriteLine();
                    WriteLine("Users: ");
                    foreach (var item in displayLogic.GetAllUsers())
                    {
                        WriteLine($"Id: {item.UserId} Name: {item.UserName} Gender: {item.Gender}");
                    }

                    WriteLine();
                    WriteLine("Institutions: ");
                    foreach (var item in displayLogic.GetAllInstitutions())
                    {
                        WriteLine($"Id: {item.InstitutionId} Name: {item.Name} Address: {item.Address}");
                    }

                    ReadKey();
                })
                .Add("Insert a dog!", () =>
                {
                    userActionDog.Insert();
                })
                .Add("Update a dog!", () =>
                {
                    userActionDog.Update();
                })
                .Add("Delete a dog!", () =>
                {
                    userActionDog.Delete();
                })
                .Add("Insert an user!", () =>
                {
                    userActionUser.Insert();
                })
                .Add("Update an user!", () =>
                {
                    userActionUser.Update();
                })
                .Add("Delete an user!", () =>
                {
                    userActionUser.Delete();
                })
                .Add("Insert an Institution!", () =>
                {
                    userActionInstitution.Insert();
                })
                .Add("Update an Institution!", () =>
                {
                    userActionInstitution.Update();
                })
                .Add("Delete an Institution!", () =>
                {
                    userActionInstitution.Delete();
                })
                .Add("Listing breeds with average age.", () =>
                {
                    IList<AverageDogAge> list = displayLogic.AverageAgesOfDogs();
                    foreach (var item in list)
                    {
                        WriteLine($"Breed: {item.DogBreed}    AverageAge: {item.AvgAge}");
                    }

                    ReadKey();
                })
                .Add("Listing users and sum their ids.", () =>
                {
                    IList<UserNamesWithIdSum> list = displayLogic.UserNamesWithIdSum();
                    foreach (var item in list)
                    {
                        WriteLine($"Name: {item.Name} Sum of ids: {item.Sum}");
                    }

                    ReadKey();
                })
                .Add("Lists and counts dogs whether they are available or not.", () =>
                {
                    IList<AvailableDog> list = displayLogic.AvailableDogs();
                    foreach (var item in list)
                    {
                        WriteLine($"Isavailable: {item.IsAvailable} Count: {item.Count}");
                    }

                    ReadKey();
                })
                .Add("Listing breeds with average age with Async method.", () =>
                {
                    var list = displayLogic.AverageAgesOfDogsAsync();
                    list.Wait();
                    foreach (var item in list.Result)
                    {
                        WriteLine($"Breed: {item.DogBreed}    AverageAge: {item.AvgAge}");
                    }

                    ReadKey();
                })
                .Add("Listing users and sum their ids with Async method", () =>
                {
                    var list = displayLogic.UserNamesWithIdSumAsync();
                    list.Wait();
                    foreach (var item in list.Result)
                    {
                        WriteLine($"Name: {item.Name} Sum of ids: {item.Sum}");
                    }

                    ReadKey();
                })
                .Add("Lists and counts dogs whether they are available or not with Async method", () =>
                {
                    var list = displayLogic.AvailableDogsAsync();
                    list.Wait();
                    foreach (var item in list.Result)
                    {
                        WriteLine($"Isavailable: {item.IsAvailable} Count: {item.Count}");
                    }

                    ReadKey();
                })
                .Add("Exit", ConsoleMenu.Close);
            menu.Show();
            context.Dispose();
        }
    }
}