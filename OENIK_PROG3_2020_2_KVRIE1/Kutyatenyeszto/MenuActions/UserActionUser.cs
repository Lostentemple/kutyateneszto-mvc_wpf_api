// <copyright file="UserActionUser.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.MenuActions
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Resources;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Logic;
    using Kutyatenyeszto.Properties;
    using Kutyatenyeszto.Repository;
    using static System.Console;

    /// <summary>
    /// Class for all the user CRUD actions.
    /// </summary>
    public class UserActionUser : CrudProvider
    {
        private static readonly string[] MagicStringsToDisplay = new string[7]
        {
            "UserName: ",
            "Password: ",
            "BirthDate: ",
            "Gender (0 = Male, 1 = Female): ",
            "EmailAddress: ",
            "PhoneNumber: ",
            "LastLoginDate: ",
        };

        private readonly IWorkWithDatabase context;
        private readonly DisplayLogic displayLogic;
        private readonly ManagementLogic managementLogic;
        private readonly ResourceManager stringManager = new ResourceManager(typeof(Resources));

        /// <summary>
        /// Initializes a new instance of the <see cref="UserActionUser"/> class.
        /// </summary>
        /// <param name="context">IWorkWithDatabase type element.</param>
        /// <param name="displayLogic">DisplayLogic type element.</param>
        /// <param name="managementLogic">ManagementLogic type element.</param>
        public UserActionUser(IWorkWithDatabase context, DisplayLogic displayLogic, ManagementLogic managementLogic)
        {
            this.context = context;
            this.displayLogic = displayLogic;
            this.managementLogic = managementLogic;
        }

        /// <summary>
        /// Returns a user.
        /// </summary>
        /// <param name="id">int type element.</param>
        public override void GetById(int id)
        {
            WriteLine(this.stringManager.GetString("askForInput", CultureInfo.CurrentUICulture));
            try
            {
                int whatToReturn = int.Parse(ReadLine() ?? " ", CultureInfo.CurrentCulture);
                this.displayLogic.GetOneUser(whatToReturn);
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Inserts a user.
        /// </summary>
        public override void Insert()
        {
            try
            {
                string queryString = string.Empty;
                foreach (var messageToDisplay in MagicStringsToDisplay)
                {
                    queryString += MakeAQueryString(messageToDisplay);
                }

                string[] cutQueryString = queryString.Split(',');

                var userToBeInserted = MakeAnUserWithCurrentData(cutQueryString, false);
                this.managementLogic.InsertUser(userToBeInserted);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Updates a user.
        /// </summary>
        public override void Update()
        {
            try
            {
                string queryString = string.Empty;
                WriteLine(this.stringManager.GetString("inputId", CultureInfo.CurrentUICulture));
                queryString += ReadLine() + ',';
                foreach (var messageToDisplay in MagicStringsToDisplay)
                {
                    queryString += MakeAQueryString(messageToDisplay);
                }

                string[] cutQueryString = queryString.Split(',');

                var userId = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture);

                var userWantToBeInserted = MakeAnUserWithCurrentData(cutQueryString, true);
                this.managementLogic.UpdateUser(userWantToBeInserted, userId);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Deletes a user.
        /// </summary>
        public override void Delete()
        {
            WriteLine(this.stringManager.GetString("askForInput", CultureInfo.CurrentUICulture));
            try
            {
                int userToBeDeleted = int.Parse(ReadLine(), CultureInfo.CurrentCulture);
                this.managementLogic.DeleteUser(userToBeDeleted);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        private static string MakeAQueryString(string message)
        {
            WriteLine(message);
            return ReadLine() + ",";
        }

        private static User MakeAnUserWithCurrentData(string[] cutQueryString, bool isUpdate)
        {
            if (isUpdate)
            {
                return new User
                {
                    UserId = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture),
                    UserName = cutQueryString[1],
                    Password = cutQueryString[2],
                    BirthDate = DateTime.Now,
                    Gender = (GenderTypes)Enum.Parse(typeof(GenderTypes), cutQueryString[4], true),
                    EmailAddress = cutQueryString[5],
                    PhoneNumber = cutQueryString[6],
                    LastLoginDate = DateTime.Now,
                };
            }
            else
            {
                return new User
                {
                    UserName = cutQueryString[0],
                    Password = cutQueryString[1],
                    BirthDate = DateTime.Now,
                    Gender = (GenderTypes)Enum.Parse(typeof(GenderTypes), cutQueryString[3], true),
                    EmailAddress = cutQueryString[4],
                    PhoneNumber = cutQueryString[5],
                    LastLoginDate = DateTime.Now,
                };
            }
        }
    }
}