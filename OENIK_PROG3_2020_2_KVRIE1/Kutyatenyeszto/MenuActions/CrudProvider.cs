// <copyright file="CrudProvider.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.MenuActions
{
    using ConsoleTools;

    /// <summary>
    /// An abstract class to provide the derived class the method signature for correct implementation.
    /// </summary>
    public abstract class CrudProvider
    {
        /// <summary>
        /// GetById method signature declaration.
        /// </summary>
        /// <param name="id">int type element.</param>
        public abstract void GetById(int id);

        /// <summary>
        /// Insert method signature declaration.
        /// </summary>
        public abstract void Insert();

        /// <summary>
        /// Update method signature declaration.
        /// </summary>
        public abstract void Update();

        /// <summary>
        /// Delete method signature declaration.
        /// </summary>
        public abstract void Delete();
    }
}