// <copyright file="UserActionDog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.MenuActions
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Resources;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Logic;
    using Kutyatenyeszto.Properties;
    using Kutyatenyeszto.Repository;
    using static System.Console;

    /// <summary>
    /// Class for all the dog CRUD actions.
    /// </summary>
    public class UserActionDog : CrudProvider
    {
        private static readonly string[] MagicStringsToDisplay = new string[7]
        {
            "Age: ",
            "Breed: ",
            "Name: ",
            "Has owner (true or false): ",
            "Fur type (0 = Shorthair. 1 = Longhair): ",
            "Is accessible (true or false): ",
            "Is uncastrated (true or false): ",
        };

        private readonly IWorkWithDatabase context;
        private readonly DisplayLogic displayLogic;
        private readonly ManagementLogic managementLogic;
        private readonly ResourceManager stringManager = new ResourceManager(typeof(Resources));

        /// <summary>
        /// Initializes a new instance of the <see cref="UserActionDog"/> class.
        /// </summary>
        /// <param name="context">IWorkWithDatabase type element.</param>
        /// <param name="displayLogic">DisplayLogic type element.</param>
        /// <param name="managementLogic">ManagementLogic type element.</param>
        public UserActionDog(IWorkWithDatabase context, DisplayLogic displayLogic, ManagementLogic managementLogic)
        {
            this.context = context;
            this.displayLogic = displayLogic;
            this.managementLogic = managementLogic;
        }

        /// <summary>
        /// Returns one dog by id.
        /// </summary>
        /// <param name="id">int type element.</param>
        public override void GetById(int id)
        {
            WriteLine(this.stringManager.GetString("askForInput", CultureInfo.CurrentUICulture));
            try
            {
                int whatToReturn = int.Parse(ReadLine() ?? " ", CultureInfo.CurrentCulture);
                this.displayLogic.GetOneDog(whatToReturn);
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Inserts a new dog.
        /// </summary>
        public override void Insert()
        {
            try
            {
                string queryString = string.Empty;
                foreach (var messageToDisplay in MagicStringsToDisplay)
                {
                    queryString += MakeAQueryString(messageToDisplay);
                }

                string[] cutQueryString = queryString.Split(',');

                var dogWantToBeInserted = MakeADogWithCurrentData(cutQueryString, false);
                this.managementLogic.InsertDog(dogWantToBeInserted);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Updates a dog.
        /// </summary>
        public override void Update()
        {
            try
            {
                string queryString = string.Empty;
                WriteLine(this.stringManager.GetString("inputId", CultureInfo.CurrentUICulture));
                queryString += ReadLine() + ',';
                foreach (var messageToDisplay in MagicStringsToDisplay)
                {
                    queryString += MakeAQueryString(messageToDisplay);
                }

                string[] cutQueryString = queryString.Split(',');

                var dogId = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture);

                var dogWantToBeUpdated = MakeADogWithCurrentData(cutQueryString, true);
                this.managementLogic.UpdateDog(dogWantToBeUpdated, dogId);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Deletes a dog.
        /// </summary>
        public override void Delete()
        {
            WriteLine(this.stringManager.GetString("askForInput", CultureInfo.CurrentUICulture));
            try
            {
                int whatToDelete = int.Parse(ReadLine() ?? " ", CultureInfo.CurrentCulture);
                this.managementLogic.DeleteDog(whatToDelete);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        private static string MakeAQueryString(string message)
        {
            WriteLine(message);
            return ReadLine() + ",";
        }

        private static Dog MakeADogWithCurrentData(string[] cutQueryString, bool isUpdate)
        {
            if (isUpdate)
            {
                return new Dog
                {
                    DogId = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture),
                    Age = int.Parse(cutQueryString[1], CultureInfo.CurrentCulture),
                    Breed = cutQueryString[2],
                    Name = cutQueryString[3],
                    HasOwner = bool.Parse(cutQueryString[4]),
                    FurType = (Furtypes)Enum.Parse(typeof(Furtypes), cutQueryString[5], true),
                    IsAccessible = bool.Parse(cutQueryString[6]),
                    IsUncastrated = bool.Parse(cutQueryString[7]),
                };
            }
            else
            {
                return new Dog
                {
                    Age = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture),
                    Breed = cutQueryString[1],
                    Name = cutQueryString[2],
                    HasOwner = bool.Parse(cutQueryString[3]),
                    FurType = (Furtypes)Enum.Parse(typeof(Furtypes), cutQueryString[4], true),
                    IsAccessible = bool.Parse(cutQueryString[5]),
                    IsUncastrated = bool.Parse(cutQueryString[6]),
                };
            }
        }
    }
}