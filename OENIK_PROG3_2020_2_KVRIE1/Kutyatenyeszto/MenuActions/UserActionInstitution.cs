// <copyright file="UserActionInstitution.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.MenuActions
{
    using System;
    using System.Globalization;
    using System.Linq;
    using System.Resources;
    using Kutyatenyeszto.Data.Models;
    using Kutyatenyeszto.Logic;
    using Kutyatenyeszto.Properties;
    using Kutyatenyeszto.Repository;
    using static System.Console;

    /// <summary>
    /// Class for all the insitution CRUD actions.
    /// </summary>
    public class UserActionInstitution : CrudProvider
    {
        private static readonly string[] MagicStringsToDisplay = new string[5]
        {
            "Name: ",
            "Address: ",
            "PhoneNumber: ",
            "Director: ",
            "Capacity: ",
        };

        private readonly IWorkWithDatabase context;
        private readonly DisplayLogic displayLogic;
        private readonly ManagementLogic managementLogic;
        private readonly ResourceManager stringManager = new ResourceManager(typeof(Resources));

        /// <summary>
        /// Initializes a new instance of the <see cref="UserActionInstitution"/> class.
        /// </summary>
        /// <param name="context">IWorkWithDatabase type element.</param>
        /// <param name="displayLogic">DisplayLogic type element.</param>
        /// <param name="managementLogic">ManagementLogic type element.</param>
        public UserActionInstitution(IWorkWithDatabase context, DisplayLogic displayLogic, ManagementLogic managementLogic)
        {
            this.context = context;
            this.displayLogic = displayLogic;
            this.managementLogic = managementLogic;
        }

        /// <summary>
        /// Returns an institution.
        /// </summary>
        /// <param name="id">int type element.</param>
        public override void GetById(int id)
        {
            WriteLine(this.stringManager.GetString("askForInput", CultureInfo.CurrentUICulture));
            try
            {
                int whatToReturn = int.Parse(ReadLine() ?? " ", CultureInfo.CurrentCulture);
                this.displayLogic.GetOneInstitution(whatToReturn);
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Inserts an insitution.
        /// </summary>
        public override void Insert()
        {
            try
            {
                string queryString = string.Empty;
                foreach (var messageToDisplay in MagicStringsToDisplay)
                {
                    queryString += MakeAQueryString(messageToDisplay);
                }

                string[] cutQueryString = queryString.Split(',');

                var institutionToBeInserted = MakeAnInstitutionWithCurrentData(cutQueryString, false);
                this.managementLogic.InsertInstitution(institutionToBeInserted);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Updates an insitution.
        /// </summary>
        public override void Update()
        {
            try
            {
                string queryString = string.Empty;
                WriteLine(this.stringManager.GetString("inputId", CultureInfo.CurrentUICulture));
                queryString += ReadLine() + ',';
                foreach (var messageToDisplay in MagicStringsToDisplay)
                {
                    queryString += MakeAQueryString(messageToDisplay);
                }

                string[] cutQueryString = queryString.Split(',');

                var userId = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture);

                var institutionWantToBeInserted = MakeAnInstitutionWithCurrentData(cutQueryString, true);
                this.managementLogic.UpdateInstitution(institutionWantToBeInserted, userId);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        /// <summary>
        /// Deletes an insitution.
        /// </summary>
        public override void Delete()
        {
            WriteLine(this.stringManager.GetString("askForInput", CultureInfo.CurrentUICulture));
            try
            {
                int institutionToBeDeleted = int.Parse(ReadLine(), CultureInfo.CurrentCulture);
                this.managementLogic.DeleteInstitution(institutionToBeDeleted);
                WriteLine(this.stringManager.GetString("successMsg", CultureInfo.CurrentUICulture));
            }
            catch (ArgumentNullException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (FormatException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
            catch (OverflowException)
            {
                WriteLine(
                    this.stringManager.GetString("errorMsg", CultureInfo.CurrentUICulture));
                ReadKey();
            }
        }

        private static string MakeAQueryString(string message)
        {
            WriteLine(message);
            return ReadLine() + ",";
        }

        private static Institution MakeAnInstitutionWithCurrentData(string[] cutQueryString, bool isUpdate)
        {
            if (isUpdate)
            {
                return new Institution
                {
                    InstitutionId = int.Parse(cutQueryString[0], CultureInfo.CurrentCulture),
                    Name = cutQueryString[1],
                    Address = cutQueryString[2],
                    PhoneNumber = cutQueryString[3],
                    Director = cutQueryString[4],
                    Capacity = int.Parse(cutQueryString[5], CultureInfo.CurrentCulture),
                };
            }
            else
            {
                return new Institution
                {
                    Name = cutQueryString[0],
                    Address = cutQueryString[1],
                    PhoneNumber = cutQueryString[2],
                    Director = cutQueryString[3],
                    Capacity = int.Parse(cutQueryString[4], CultureInfo.CurrentCulture),
                };
            }
        }
    }
}