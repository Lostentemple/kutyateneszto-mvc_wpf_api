// <copyright file="UserConfigurations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.EntityConfigurations
{
    using System;
    using Kutyatenyeszto.Data.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Contains the fluent API overrides for User model.
    /// </summary>
    public class UserConfigurations : IEntityTypeConfiguration<User>
    {
        /// <summary>
        /// Configuring the user properties.
        /// </summary>
        /// <param name="builder">User type EntityTypeBuilder.</param>
        public void Configure(EntityTypeBuilder<User> builder)
        {
            if (builder == null)
            {
                return;
            }

            builder.Property(user => user.UserId)
                .IsRequired();

            builder.Property(user => user.UserName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(user => user.Password)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(user => user.EmailAddress)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(user => user.PhoneNumber)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(user => user.Gender)
                .IsRequired();

            builder.Property(user => user.BirthDate)
                .IsRequired()
                .HasColumnType("date");

            User us = new User()
            {
                UserId = 1,
                UserName = "Kovács Károly",
                Password = "12345",
                Gender = GenderTypes.Male,
                BirthDate = DateTime.Now,
                LastLoginDate = null,
                PhoneNumber = "123456789",
                EmailAddress = "kiskacsa@kiskacsa.hu",
            };

            User us1 = new User()
            {
                UserId = 2,
                UserName = "Süveg Sára",
                Password = "54321",
                Gender = GenderTypes.Female,
                BirthDate = DateTime.Now,
                LastLoginDate = null,
                PhoneNumber = "987654321",
                EmailAddress = "nokedli@nokedli.hu",
            };
            builder.HasData(us, us1);
        }
    }
}