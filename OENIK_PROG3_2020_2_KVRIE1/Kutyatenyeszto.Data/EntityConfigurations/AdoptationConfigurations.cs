// <copyright file="AdoptationConfigurations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.EntityConfigurations
{
    using Kutyatenyeszto.Data.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Contains the fluent API overrides for Adoptation model.
    /// </summary>
    public class AdoptationConfigurations : IEntityTypeConfiguration<Adoptation>
    {
        /// <summary>
        /// Configuring the dog properties.
        /// </summary>
        /// <param name="builder">Dog type EntityTypeBuilder.</param>
        public void Configure(EntityTypeBuilder<Adoptation> builder)
        {
            if (builder == null)
            {
                return;
            }

            builder.Property(adoptation => adoptation.AdoptationId)
                .IsRequired();

            builder.HasOne(adoptation => adoptation.Dog).WithOne();
            builder.HasOne(adoptation => adoptation.User).WithOne();
            builder.HasOne(adoptation => adoptation.Institution).WithOne();

            Adoptation adoptation1 = new Adoptation()
            {
                AdoptationId = 1,
                DogId = 1,
                UserId = 1,
                InstitutionId = 1,
            };
            Adoptation adoptation2 = new Adoptation()
            {
                AdoptationId = 2,
                DogId = 2,
                UserId = 2,
                InstitutionId = 2,
            };
            builder.HasData(adoptation1, adoptation2);
        }
    }
}