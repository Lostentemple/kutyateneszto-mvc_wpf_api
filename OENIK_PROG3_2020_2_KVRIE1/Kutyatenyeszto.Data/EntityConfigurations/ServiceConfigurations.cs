// <copyright file="ServiceConfigurations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.EntityConfigurations
{
    using System;
    using Kutyatenyeszto.Data.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Contains the fluent API overrides for Service model.
    /// </summary>
    public class ServiceConfigurations : IEntityTypeConfiguration<Service>
    {
        /// <summary>
        /// Configuring the service properties.
        /// </summary>
        /// <param name="builder">Service type EntityTypeBuilder.</param>
        public void Configure(EntityTypeBuilder<Service> builder)
        {
            if (builder == null)
            {
                return;
            }

            builder.Property(service => service.ServicesId)
                .IsRequired();
            builder.Property(service => service.ServiceName)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(service => service.Type)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(service => service.Price)
                .IsRequired();
            builder.Property(service => service.ServiceProvider)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(service => service.ServiceDate)
                .IsRequired()
                .HasColumnType("date");
            Service srv1 = new Service()
            {
                ServicesId = 1,
                ServiceName = "Fodrászolás",
                Type = "Teljes",
                Price = 20000,
                ServiceProvider = "Józsi",
                ServiceDate = DateTime.Now,
            };
            Service srv2 = new Service()
            {
                ServicesId = 2,
                ServiceName = "Fésülés",
                Type = "Teljes",
                Price = 10000,
                ServiceProvider = "Erika",
                ServiceDate = DateTime.Now,
            };
            builder.HasData(srv1, srv2);
        }
    }
}