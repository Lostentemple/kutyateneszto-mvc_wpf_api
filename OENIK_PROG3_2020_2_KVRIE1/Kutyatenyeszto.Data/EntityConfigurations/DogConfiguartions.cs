// <copyright file="DogConfiguartions.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.EntityConfigurations
{
    using Kutyatenyeszto.Data.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Contains the fluent API overrides for Dog model.
    /// </summary>
    public class DogConfiguartions : IEntityTypeConfiguration<Dog>
    {
        /// <summary>
        /// Configuring the dog properties.
        /// </summary>
        /// <param name="builder">Dog type EntityTypeBuilder.</param>
        public void Configure(EntityTypeBuilder<Dog> builder)
        {
            if (builder == null)
            {
                return;
            }

            builder.Property(dog => dog.DogId)
                .IsRequired();

            builder.Property(dog => dog.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(dog => dog.Breed)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);

            builder.Property(dog => dog.Age)
                .IsRequired();

            builder.Property(dog => dog.IsUncastrated)
                .IsRequired();

            builder.Property(dog => dog.IsAccessible)
                .IsRequired();

            builder.Property(dog => dog.FurType)
                .IsRequired();

            builder.Property(dog => dog.HasOwner)
                .IsRequired();

            Dog dog1 = new Dog()
            {
                DogId = 1,
                Name = "Bodri",
                Breed = "Skótjuhász",
                FurType = Furtypes.Longhair,
                Age = 5,
                IsUncastrated = false,
                HasOwner = true,
                IsAccessible = true,
            };
            Dog dog2 = new Dog()
            {
                DogId = 2,
                Name = "Villám",
                Breed = "Vizsla",
                FurType = Furtypes.Shorthair,
                Age = 4,
                IsUncastrated = true,
                HasOwner = true,
                IsAccessible = false,
            };
            builder.HasData(dog1, dog2);
        }
    }
}