// <copyright file="InstitutionConfigurations.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.EntityConfigurations
{
    using Kutyatenyeszto.Data.Models;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.EntityFrameworkCore.Metadata.Builders;

    /// <summary>
    /// Contains the fluent API overrides for Insitution model.
    /// </summary>
    public class InstitutionConfigurations : IEntityTypeConfiguration<Institution>
    {
        /// <summary>
        /// Configuring the institution properties.
        /// </summary>
        /// <param name="builder">Institution type EntityTypeBuilder.</param>
        public void Configure(EntityTypeBuilder<Institution> builder)
        {
            if (builder == null)
            {
                return;
            }

            builder.Property(institution => institution.InstitutionId)
                .IsRequired();
            builder.Property(institution => institution.Name)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(institution => institution.Address)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(institution => institution.PhoneNumber)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(institution => institution.Director)
                .IsRequired()
                .HasMaxLength(50)
                .IsUnicode(false);
            builder.Property(institution => institution.Capacity)
                .IsRequired();
            Institution inst1 = new Institution()
            {
                InstitutionId = 1,
                Name = "Rex Állatotthon",
                Address = "Fő utca 13",
                PhoneNumber = "11122233",
                Director = "Kiss Jakab",
                Capacity = 40,
                Selected = false,
            };
            Institution inst2 = new Institution()
            {
                InstitutionId = 2,
                Name = "Kiskedvenc Állatotthon",
                Address = "Lapát köz 2",
                PhoneNumber = "464642221",
                Director = "Vörös Vivienn",
                Capacity = 20,
                Selected = false,
            };
            builder.HasData(inst1, inst2);
        }
    }
}