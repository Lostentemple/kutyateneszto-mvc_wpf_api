// <copyright file="Adoptation.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.Models
{
    /// <summary>
    /// Class that contains all properties of Adoptations.
    /// </summary>
    public class Adoptation
    {
        /// <summary>
        /// Gets or sets primary key to identify adoptations.
        /// </summary>
        public int AdoptationId { get; set; }

        /// <summary>
        /// Gets or sets an identifier for users.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets an identifier for dogs.
        /// </summary>
        public int DogId { get; set; }

        /// <summary>
        /// Gets or sets an identifier for institutions.
        /// </summary>
        public int InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets navigation property for users.
        /// </summary>
        public virtual User User { get; set; }

        /// <summary>
        /// Gets or sets navigation property for dogs.
        /// </summary>
        public virtual Dog Dog { get; set; }

        /// <summary>
        /// Gets or sets navigation property for institutions.
        /// </summary>
        public virtual Institution Institution { get; set; }
    }
}