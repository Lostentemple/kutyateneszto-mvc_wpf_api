// <copyright file="Service.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.Models
{
    using System;

    /// <summary>
    /// Class that contains all properties of Service.
    /// </summary>
    public class Service
    {
        /// <summary>
        /// Gets or sets primary key to identify services.
        /// </summary>
        public int ServicesId { get; set; }

        /// <summary>
        /// Gets or sets service name for each service.
        /// </summary>
        public string ServiceName { get; set; }

        /// <summary>
        /// Gets or sets price for each service.
        /// </summary>
        public int Price { get; set; }

        /// <summary>
        /// Gets or sets type for each service.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets service provider for each service.
        /// </summary>
        public string ServiceProvider { get; set; }

        /// <summary>
        ///  Gets or sets service date for each service.
        /// </summary>
        public DateTime ServiceDate { get; set; }

        // public int UserId { get; set; }
        // public int DogId { get; set; }
    }
}