// <copyright file="Dog.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.Models
{
    /// <summary>
    /// Define enum for Fur types.
    /// </summary>
    public enum Furtypes
    {
        /// <summary>
        /// Short hair enum item.
        /// </summary>
        Shorthair,

        /// <summary>
        /// Long hair enum item.
        /// </summary>
        Longhair,
    }

    /// <summary>
    /// Class that contains all properties of Dog.
    /// </summary>
    public class Dog
    {
        /// <summary>
        /// Gets or sets primary key to identify dogs.
        /// </summary>
        public int DogId { get; set; }

        /// <summary>
        /// Gets or sets name for the user.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets age for dogs.
        /// </summary>
        public int Age { get; set; }

        /// <summary>
        /// Gets or sets breed for dogs.
        /// </summary>
        public string Breed { get; set; }

        /// <summary>
        /// Gets or sets fur type for dogs.
        /// </summary>
        public Furtypes FurType { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the dog is uncastrated.
        /// </summary>
        public bool IsUncastrated { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the dog is accessible.
        /// </summary>
        public bool IsAccessible { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether the dog has an owner.
        /// </summary>
        public bool HasOwner { get; set; }
    }
}