// <copyright file="Institution.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.Models
{
    /// <summary>
    /// Class that contains all properties of institution.
    /// </summary>
    public class Institution
    {
        /// <summary>
        /// Gets or sets primary key to identify users.
        /// </summary>
        public int InstitutionId { get; set; }

        /// <summary>
        /// Gets or sets name for the institution.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Gets or sets address for the institution.
        /// </summary>
        public string Address { get; set; }

        /// <summary>
        /// Gets or sets phone number for the institution.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets director for the institution.
        /// </summary>
        public string Director { get; set; }

        /// <summary>
        /// Gets or sets capacity for the institution.
        /// </summary>
        public int Capacity { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether gets or sets selected prop for the institution.
        /// </summary>
        public bool Selected { get; set; }
    }
}