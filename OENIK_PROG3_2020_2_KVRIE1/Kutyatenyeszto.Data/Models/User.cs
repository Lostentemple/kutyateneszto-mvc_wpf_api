// <copyright file="User.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data.Models
{
    using System;

    /// <summary>
    /// Define enum for genders.
    /// </summary>
    public enum GenderTypes
    {
        /// <summary>
        /// Male enum item.
        /// </summary>
        Male,

        /// <summary>
        /// Female enum item.
        /// </summary>
        Female,
    }

    /// <summary>
    /// Class that contains all properties of User.
    /// </summary>
    public class User
    {
        /// <summary>
        /// Gets or sets primary key to identify users.
        /// </summary>
        public int UserId { get; set; }

        /// <summary>
        /// Gets or sets username for the user.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Gets or sets password for the user.
        /// </summary>
        public string Password { get; set; }

        /// <summary>
        /// Gets or sets birthdate for the user.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Gets or sets gender for the user.
        /// </summary>
        public GenderTypes Gender { get; set; }

        /// <summary>
        /// Gets or sets email address for the user.
        /// </summary>
        public string EmailAddress { get; set; }

        /// <summary>
        /// Gets or sets phone number for the user.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Gets or sets last login date for user.
        /// </summary>
        public DateTime? LastLoginDate { get; set; }
    }
}