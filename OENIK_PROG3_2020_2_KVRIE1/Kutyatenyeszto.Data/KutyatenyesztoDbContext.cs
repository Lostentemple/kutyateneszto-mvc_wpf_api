﻿// <copyright file="KutyatenyesztoDbContext.cs" company="PlaceholderCompany">
// Copyright (c) PlaceholderCompany. All rights reserved.
// </copyright>

namespace Kutyatenyeszto.Data
{
    using Kutyatenyeszto.Data.EntityConfigurations;
    using Kutyatenyeszto.Data.Models;
    using Microsoft.EntityFrameworkCore;

    /// <summary>
    /// Initialize and connect database.
    /// </summary>
    public sealed class KutyatenyesztoDbContext : DbContext
    {
        private static KutyatenyesztoDbContext singleton;

        /// <summary>
        /// Initializes a new instance of the <see cref="KutyatenyesztoDbContext"/> class.
        /// Ensures that the database for the context exists.
        /// </summary>
        public KutyatenyesztoDbContext()
        {
            this.Database.EnsureCreated();
        }

        /// <summary>
        /// Gets a singleton instance of the db context.
        /// </summary>
        public static KutyatenyesztoDbContext SingletonDbContext
        {
            get
            {
                if (singleton == null)
                {
                    singleton = new KutyatenyesztoDbContext();
                }

                return singleton;
            }
        }

        /// <summary>
        /// Gets or sets user elements.
        /// </summary>
        public DbSet<User> Users { get; set; }

        /// <summary>
        /// Gets or sets dog elements.
        /// </summary>
        public DbSet<Dog> Dogs { get; set; }

        // /// <summary>
        // /// Gets or sets service elements.
        // /// </summary>
        // public DbSet<Service> Services { get; set; }

        /// <summary>
        /// Gets or sets service elements.
        /// </summary>
        public DbSet<Institution> Institutions { get; set; }

        /// <summary>
        /// Gets or sets Adoptation elements.
        /// </summary>
        public DbSet<Adoptation> Adoptations { get; set; }

        /// <summary>
        /// Turns on the creation of lazy-loading proxies and configures the context to connect to a Microsoft SQL Server database.
        /// </summary>
        /// <param name="optionsBuilder">DbContextOptionsBuilder type.</param>
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder
                .UseLazyLoadingProxies()
                .UseSqlServer(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\KutyatenyesztoDb.mdf;Integrated Security=True");
        }

        /// <summary>
        /// Method overrides entity framework core default conventions.
        /// </summary>
        /// <param name="modelBuilder">ModelBuilder type element.</param>
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder?
                .ApplyConfiguration(new UserConfigurations())
                .ApplyConfiguration(new DogConfiguartions())
                .ApplyConfiguration(new InstitutionConfigurations())
                .ApplyConfiguration(new AdoptationConfigurations());
        }
    }
}
