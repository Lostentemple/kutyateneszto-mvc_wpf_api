var class_kutyatenyeszto_1_1_data_1_1_models_1_1_user =
[
    [ "BirthDate", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a0bceea3d4b548e829eb764f87cfe6e54", null ],
    [ "EmailAddress", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a8e3fc417ce12882356e7ffa994160012", null ],
    [ "Gender", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#acd1212ee6c3ca0305b6fe492aad2cbfe", null ],
    [ "LastLoginDate", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a8936b32838a9c37d2c643b65b7be9c26", null ],
    [ "Password", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a51268f940b8cac019b28c3c9ab0d0938", null ],
    [ "PhoneNumber", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a4846bdda56c28dfecdd2b5049113ae5b", null ],
    [ "UserId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a73243287c510d5faaf8644faebc2a790", null ],
    [ "UserName", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html#a53bfc3426c715701d9a6e7d627548bfc", null ]
];