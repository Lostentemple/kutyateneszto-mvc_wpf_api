var interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic =
[
    [ "DeleteDog", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#ac5942d4058e479971c0ba08072b53142", null ],
    [ "DeleteInstitution", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#ac23b13f0998bf9c3ef4d96374ec69306", null ],
    [ "DeleteUser", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#aac6525592c3e050c2103299a67006bc1", null ],
    [ "InsertDog", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#aa5b09b1b46c1c5801a9e9f356b632bf1", null ],
    [ "InsertInstitution", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#ac43505ab557a43f2b165f14c1b8813fc", null ],
    [ "InsertUser", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#a5f2e3079a7141ec263090e7c87290853", null ],
    [ "UpdateDog", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#aca9466f36994441e61a704f05ca4282a", null ],
    [ "UpdateInstitution", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#a1291991f9aab2efc5ba687b95032fb33", null ],
    [ "UpdateUser", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html#a769f677c5cc3d136478ab7f494e93f47", null ]
];