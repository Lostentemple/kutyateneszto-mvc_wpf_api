var annotated_dup =
[
    [ "AspNetCore", "namespace_asp_net_core.html", [
      [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
      [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
      [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
      [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
      [ "Views_Institutions_InstitutionDetails", "class_asp_net_core_1_1_views___institutions___institution_details.html", "class_asp_net_core_1_1_views___institutions___institution_details" ],
      [ "Views_Institutions_InstitutionEdit", "class_asp_net_core_1_1_views___institutions___institution_edit.html", "class_asp_net_core_1_1_views___institutions___institution_edit" ],
      [ "Views_Institutions_InstitutionIndex", "class_asp_net_core_1_1_views___institutions___institution_index.html", "class_asp_net_core_1_1_views___institutions___institution_index" ],
      [ "Views_Institutions_InstitutionList", "class_asp_net_core_1_1_views___institutions___institution_list.html", "class_asp_net_core_1_1_views___institutions___institution_list" ],
      [ "Views_Random_SelectionResult", "class_asp_net_core_1_1_views___random___selection_result.html", "class_asp_net_core_1_1_views___random___selection_result" ],
      [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
      [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
      [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
    ] ],
    [ "Kutyatenyeszto", "namespace_kutyatenyeszto.html", [
      [ "Data", "namespace_kutyatenyeszto_1_1_data.html", [
        [ "EntityConfigurations", "namespace_kutyatenyeszto_1_1_data_1_1_entity_configurations.html", [
          [ "AdoptationConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_adoptation_configurations.html", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_adoptation_configurations" ],
          [ "DogConfiguartions", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_dog_configuartions.html", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_dog_configuartions" ],
          [ "InstitutionConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_institution_configurations.html", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_institution_configurations" ],
          [ "ServiceConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_service_configurations.html", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_service_configurations" ],
          [ "UserConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_user_configurations.html", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_user_configurations" ]
        ] ],
        [ "Models", "namespace_kutyatenyeszto_1_1_data_1_1_models.html", [
          [ "Adoptation", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation" ],
          [ "Dog", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog" ],
          [ "Institution", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution" ],
          [ "Service", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_service" ],
          [ "User", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user" ]
        ] ],
        [ "KutyatenyesztoDbContext", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context" ]
      ] ],
      [ "Logic", "namespace_kutyatenyeszto_1_1_logic.html", [
        [ "Tests", "namespace_kutyatenyeszto_1_1_logic_1_1_tests.html", [
          [ "DisplayLogicTest", "class_kutyatenyeszto_1_1_logic_1_1_tests_1_1_display_logic_test.html", "class_kutyatenyeszto_1_1_logic_1_1_tests_1_1_display_logic_test" ],
          [ "ManagementLogicTest", "class_kutyatenyeszto_1_1_logic_1_1_tests_1_1_management_logic_test.html", "class_kutyatenyeszto_1_1_logic_1_1_tests_1_1_management_logic_test" ]
        ] ],
        [ "AvailableDog", "class_kutyatenyeszto_1_1_logic_1_1_available_dog.html", "class_kutyatenyeszto_1_1_logic_1_1_available_dog" ],
        [ "AverageDogAge", "class_kutyatenyeszto_1_1_logic_1_1_average_dog_age.html", "class_kutyatenyeszto_1_1_logic_1_1_average_dog_age" ],
        [ "DisplayLogic", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html", "class_kutyatenyeszto_1_1_logic_1_1_display_logic" ],
        [ "IDisplayLogic", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic" ],
        [ "IManagementLogic", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic" ],
        [ "ManagementLogic", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html", "class_kutyatenyeszto_1_1_logic_1_1_management_logic" ],
        [ "UserNamesWithIdSum", "class_kutyatenyeszto_1_1_logic_1_1_user_names_with_id_sum.html", "class_kutyatenyeszto_1_1_logic_1_1_user_names_with_id_sum" ]
      ] ],
      [ "MenuActions", "namespace_kutyatenyeszto_1_1_menu_actions.html", [
        [ "CrudProvider", "class_kutyatenyeszto_1_1_menu_actions_1_1_crud_provider.html", "class_kutyatenyeszto_1_1_menu_actions_1_1_crud_provider" ],
        [ "UserActionDog", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_dog.html", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_dog" ],
        [ "UserActionInstitution", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_institution.html", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_institution" ],
        [ "UserActionUser", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_user.html", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_user" ]
      ] ],
      [ "Properties", "namespace_kutyatenyeszto_1_1_properties.html", [
        [ "Resources", "class_kutyatenyeszto_1_1_properties_1_1_resources.html", "class_kutyatenyeszto_1_1_properties_1_1_resources" ]
      ] ],
      [ "Repository", "namespace_kutyatenyeszto_1_1_repository.html", [
        [ "AdoptationRepository", "class_kutyatenyeszto_1_1_repository_1_1_adoptation_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_adoptation_repository" ],
        [ "DogRepository", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository" ],
        [ "GenericRepository", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository" ],
        [ "IAdoptationRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_adoptation_repository.html", null ],
        [ "IDogRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_dog_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_dog_repository" ],
        [ "IInstitutionRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_institution_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_institution_repository" ],
        [ "InstitutionRepository", "class_kutyatenyeszto_1_1_repository_1_1_institution_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_institution_repository" ],
        [ "IRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository" ],
        [ "IUserRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_user_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_user_repository" ],
        [ "IWorkWithDatabase", "interface_kutyatenyeszto_1_1_repository_1_1_i_work_with_database.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_work_with_database" ],
        [ "UserRepository", "class_kutyatenyeszto_1_1_repository_1_1_user_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_user_repository" ],
        [ "WorkwithDatabase", "class_kutyatenyeszto_1_1_repository_1_1_workwith_database.html", "class_kutyatenyeszto_1_1_repository_1_1_workwith_database" ]
      ] ],
      [ "WEB", "namespace_kutyatenyeszto_1_1_w_e_b.html", [
        [ "Controllers", "namespace_kutyatenyeszto_1_1_w_e_b_1_1_controllers.html", [
          [ "ApiResult", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_api_result.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_api_result" ],
          [ "HomeController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_home_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_home_controller" ],
          [ "InstitutionsApiController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_api_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_api_controller" ],
          [ "InstitutionsController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_controller" ],
          [ "NewApiResult", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_new_api_result.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_new_api_result" ],
          [ "RandomController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_random_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_random_controller" ]
        ] ],
        [ "Models", "namespace_kutyatenyeszto_1_1_w_e_b_1_1_models.html", [
          [ "ErrorViewModel", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_error_view_model.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_error_view_model" ],
          [ "Institution", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution" ],
          [ "InstitutionListViewModel", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution_list_view_model.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution_list_view_model" ],
          [ "MapperFactory", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_mapper_factory.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_mapper_factory" ]
        ] ],
        [ "Program", "class_kutyatenyeszto_1_1_w_e_b_1_1_program.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_program" ],
        [ "Startup", "class_kutyatenyeszto_1_1_w_e_b_1_1_startup.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_startup" ]
      ] ],
      [ "WPF", "namespace_kutyatenyeszto_1_1_w_p_f.html", [
        [ "BL", "namespace_kutyatenyeszto_1_1_w_p_f_1_1_b_l.html", [
          [ "IEditorService", "interface_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", "interface_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_i_editor_service" ],
          [ "IUserLogic", "interface_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_i_user_logic.html", "interface_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_i_user_logic" ],
          [ "UserLogic", "class_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_user_logic.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_user_logic" ]
        ] ],
        [ "Data", "namespace_kutyatenyeszto_1_1_w_p_f_1_1_data.html", [
          [ "UserModel", "class_kutyatenyeszto_1_1_w_p_f_1_1_data_1_1_user_model.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_data_1_1_user_model" ]
        ] ],
        [ "UI", "namespace_kutyatenyeszto_1_1_w_p_f_1_1_u_i.html", [
          [ "EditorWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window" ],
          [ "Window1", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_window1.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_window1" ],
          [ "EditorServiceViaWindows", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_windows.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_windows" ]
        ] ],
        [ "VM", "namespace_kutyatenyeszto_1_1_w_p_f_1_1_v_m.html", [
          [ "EditorViewModel", "class_kutyatenyeszto_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_v_m_1_1_editor_view_model" ],
          [ "MainViewModel", "class_kutyatenyeszto_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_v_m_1_1_main_view_model" ]
        ] ],
        [ "App", "class_kutyatenyeszto_1_1_w_p_f_1_1_app.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_app" ],
        [ "MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window" ],
        [ "MyIoc", "class_kutyatenyeszto_1_1_w_p_f_1_1_my_ioc.html", "class_kutyatenyeszto_1_1_w_p_f_1_1_my_ioc" ]
      ] ],
      [ "WpfClient", "namespace_kutyatenyeszto_1_1_wpf_client.html", [
        [ "App", "class_kutyatenyeszto_1_1_wpf_client_1_1_app.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_app" ],
        [ "EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window" ],
        [ "IMainLogic", "interface_kutyatenyeszto_1_1_wpf_client_1_1_i_main_logic.html", "interface_kutyatenyeszto_1_1_wpf_client_1_1_i_main_logic" ],
        [ "InstitutionVM", "class_kutyatenyeszto_1_1_wpf_client_1_1_institution_v_m.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_institution_v_m" ],
        [ "MainLogic", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_logic.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_logic" ],
        [ "MainVM", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_v_m.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_v_m" ],
        [ "MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window" ],
        [ "MyIoc", "class_kutyatenyeszto_1_1_wpf_client_1_1_my_ioc.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_my_ioc" ]
      ] ],
      [ "WPFRandom", "namespace_kutyatenyeszto_1_1_w_p_f_random.html", [
        [ "VM", "namespace_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m.html", [
          [ "InstitutionVM", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_institution_v_m.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_institution_v_m" ],
          [ "ListVM", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_list_v_m.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_list_v_m" ],
          [ "MainVM", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_main_v_m.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_main_v_m" ]
        ] ],
        [ "App", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_app.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_app" ],
        [ "ListWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window" ],
        [ "MainLogic", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_logic.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_logic" ],
        [ "MainWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window" ],
        [ "NewApiResult", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_new_api_result.html", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_new_api_result" ]
      ] ],
      [ "Program", "class_kutyatenyeszto_1_1_program.html", null ]
    ] ],
    [ "XamlGeneratedNamespace", "namespace_xaml_generated_namespace.html", [
      [ "GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", "class_xaml_generated_namespace_1_1_generated_internal_type_helper" ]
    ] ]
];