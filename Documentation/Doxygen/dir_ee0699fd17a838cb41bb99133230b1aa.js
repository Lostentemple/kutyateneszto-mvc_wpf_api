var dir_ee0699fd17a838cb41bb99133230b1aa =
[
    [ "obj", "dir_c51cab79f28290d30cbcd25975e89c54.html", "dir_c51cab79f28290d30cbcd25975e89c54" ],
    [ "AdoptationRepository.cs", "_adoptation_repository_8cs_source.html", null ],
    [ "DogRepository.cs", "_dog_repository_8cs_source.html", null ],
    [ "GenericRepository.cs", "_generic_repository_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_kutyatenyeszto_8_repository_2_global_suppressions_8cs_source.html", null ],
    [ "IAdoptationRepository.cs", "_i_adoptation_repository_8cs_source.html", null ],
    [ "IDogRepository.cs", "_i_dog_repository_8cs_source.html", null ],
    [ "IInstitutionRepository.cs", "_i_institution_repository_8cs_source.html", null ],
    [ "InstitutionRepository.cs", "_institution_repository_8cs_source.html", null ],
    [ "IRepository.cs", "_i_repository_8cs_source.html", null ],
    [ "IUserRepository.cs", "_i_user_repository_8cs_source.html", null ],
    [ "IWorkWithDatabase.cs", "_i_work_with_database_8cs_source.html", null ],
    [ "UserRepository.cs", "_user_repository_8cs_source.html", null ],
    [ "WorkwithDatabase.cs", "_workwith_database_8cs_source.html", null ]
];