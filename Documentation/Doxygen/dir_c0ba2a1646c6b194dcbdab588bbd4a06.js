var dir_c0ba2a1646c6b194dcbdab588bbd4a06 =
[
    [ "obj", "dir_dfc3056667a70e1784614ba005dd2a5d.html", "dir_dfc3056667a70e1784614ba005dd2a5d" ],
    [ "AdoptationRepository.cs", "_adoptation_repository_8cs_source.html", null ],
    [ "DogRepository.cs", "_dog_repository_8cs_source.html", null ],
    [ "GenericRepository.cs", "_generic_repository_8cs_source.html", null ],
    [ "GlobalSuppressions.cs", "_global_suppressions_8cs_source.html", null ],
    [ "IAdoptationRepository.cs", "_i_adoptation_repository_8cs_source.html", null ],
    [ "IDogRepository.cs", "_i_dog_repository_8cs_source.html", null ],
    [ "IInstitutionRepository.cs", "_i_institution_repository_8cs_source.html", null ],
    [ "InstitutionRepository.cs", "_institution_repository_8cs_source.html", null ],
    [ "IRepository.cs", "_i_repository_8cs_source.html", null ],
    [ "IUserRepository.cs", "_i_user_repository_8cs_source.html", null ],
    [ "IWorkWithDatabase.cs", "_i_work_with_database_8cs_source.html", null ],
    [ "UserRepository.cs", "_user_repository_8cs_source.html", null ],
    [ "WorkwithDatabase.cs", "_workwith_database_8cs_source.html", null ]
];