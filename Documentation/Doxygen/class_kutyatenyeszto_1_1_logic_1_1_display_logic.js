var class_kutyatenyeszto_1_1_logic_1_1_display_logic =
[
    [ "DisplayLogic", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a19585378e9359a68af244d5f1ff9bc2f", null ],
    [ "AvailableDogs", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#aa45625833e5ce8a8aa6081edee093ebf", null ],
    [ "AvailableDogsAsync", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#ae401a8c58f5d326a6b70af4f10a0b21c", null ],
    [ "AverageAgesOfDogs", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a043a098b3d61d6aac1aff2bc3b89accf", null ],
    [ "AverageAgesOfDogsAsync", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#ab947381c21cc3689d7065e8961cbe886", null ],
    [ "GetAllDogs", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#aaef0df7613e786e6c250529394fa4e09", null ],
    [ "GetAllInstitutions", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a0b73ef19a847184081caa55edb230dba", null ],
    [ "GetAllUsers", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#af3df081b2b8ad5a2a7865f41920afd46", null ],
    [ "GetOneDog", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a250884b01eb635e8024f9520e3565645", null ],
    [ "GetOneInstitution", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a6e02600315e80401c158b9e598befde6", null ],
    [ "GetOneUser", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a77e56be9d9496948e7457a1a9cb2d06f", null ],
    [ "UserNamesWithIdSum", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a367dd8074d086d333df4bbd6b7ba69ec", null ],
    [ "UserNamesWithIdSumAsync", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html#a5ae2da6c76bf8c59cf677f2183ada572", null ]
];