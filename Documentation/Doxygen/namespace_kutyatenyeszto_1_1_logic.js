var namespace_kutyatenyeszto_1_1_logic =
[
    [ "Tests", "namespace_kutyatenyeszto_1_1_logic_1_1_tests.html", "namespace_kutyatenyeszto_1_1_logic_1_1_tests" ],
    [ "AvailableDog", "class_kutyatenyeszto_1_1_logic_1_1_available_dog.html", "class_kutyatenyeszto_1_1_logic_1_1_available_dog" ],
    [ "AverageDogAge", "class_kutyatenyeszto_1_1_logic_1_1_average_dog_age.html", "class_kutyatenyeszto_1_1_logic_1_1_average_dog_age" ],
    [ "DisplayLogic", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html", "class_kutyatenyeszto_1_1_logic_1_1_display_logic" ],
    [ "IDisplayLogic", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic" ],
    [ "IManagementLogic", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic" ],
    [ "ManagementLogic", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html", "class_kutyatenyeszto_1_1_logic_1_1_management_logic" ],
    [ "UserNamesWithIdSum", "class_kutyatenyeszto_1_1_logic_1_1_user_names_with_id_sum.html", "class_kutyatenyeszto_1_1_logic_1_1_user_names_with_id_sum" ]
];