var hierarchy =
[
    [ "Kutyatenyeszto.Data.Models.Adoptation", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html", null ],
    [ "Kutyatenyeszto.WEB.Controllers.ApiResult", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_api_result.html", null ],
    [ "Application", null, [
      [ "Kutyatenyeszto.WPF.App", "class_kutyatenyeszto_1_1_w_p_f_1_1_app.html", null ]
    ] ],
    [ "System.Windows.Application", null, [
      [ "Kutyatenyeszto.WPF.App", "class_kutyatenyeszto_1_1_w_p_f_1_1_app.html", null ],
      [ "Kutyatenyeszto.WPF.App", "class_kutyatenyeszto_1_1_w_p_f_1_1_app.html", null ],
      [ "Kutyatenyeszto.WPF.App", "class_kutyatenyeszto_1_1_w_p_f_1_1_app.html", null ],
      [ "Kutyatenyeszto.WPFRandom.App", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_app.html", null ],
      [ "Kutyatenyeszto.WPFRandom.App", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_app.html", null ],
      [ "Kutyatenyeszto.WPFRandom.App", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_app.html", null ],
      [ "Kutyatenyeszto.WpfClient.App", "class_kutyatenyeszto_1_1_wpf_client_1_1_app.html", null ],
      [ "Kutyatenyeszto.WpfClient.App", "class_kutyatenyeszto_1_1_wpf_client_1_1_app.html", null ],
      [ "Kutyatenyeszto.WpfClient.App", "class_kutyatenyeszto_1_1_wpf_client_1_1_app.html", null ]
    ] ],
    [ "Kutyatenyeszto.Logic.AvailableDog", "class_kutyatenyeszto_1_1_logic_1_1_available_dog.html", null ],
    [ "Kutyatenyeszto.Logic.AverageDogAge", "class_kutyatenyeszto_1_1_logic_1_1_average_dog_age.html", null ],
    [ "Controller", null, [
      [ "Kutyatenyeszto.WEB.Controllers.HomeController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_home_controller.html", null ],
      [ "Kutyatenyeszto.WEB.Controllers.InstitutionsApiController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_api_controller.html", null ],
      [ "Kutyatenyeszto.WEB.Controllers.InstitutionsController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_controller.html", null ],
      [ "Kutyatenyeszto.WEB.Controllers.RandomController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_random_controller.html", null ]
    ] ],
    [ "Kutyatenyeszto.MenuActions.CrudProvider", "class_kutyatenyeszto_1_1_menu_actions_1_1_crud_provider.html", [
      [ "Kutyatenyeszto.MenuActions.UserActionDog", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_dog.html", null ],
      [ "Kutyatenyeszto.MenuActions.UserActionInstitution", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_institution.html", null ],
      [ "Kutyatenyeszto.MenuActions.UserActionUser", "class_kutyatenyeszto_1_1_menu_actions_1_1_user_action_user.html", null ]
    ] ],
    [ "DbContext", null, [
      [ "Kutyatenyeszto.Data.KutyatenyesztoDbContext", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html", null ]
    ] ],
    [ "Kutyatenyeszto.Logic.Tests.DisplayLogicTest", "class_kutyatenyeszto_1_1_logic_1_1_tests_1_1_display_logic_test.html", null ],
    [ "Kutyatenyeszto.Data.Models.Dog", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html", null ],
    [ "Kutyatenyeszto.WEB.Models.ErrorViewModel", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_error_view_model.html", null ],
    [ "Kutyatenyeszto.Repository.GenericRepository< Adoptation >", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", [
      [ "Kutyatenyeszto.Repository.AdoptationRepository", "class_kutyatenyeszto_1_1_repository_1_1_adoptation_repository.html", null ]
    ] ],
    [ "Kutyatenyeszto.Repository.GenericRepository< Dog >", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", [
      [ "Kutyatenyeszto.Repository.DogRepository", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html", null ]
    ] ],
    [ "Kutyatenyeszto.Repository.GenericRepository< Institution >", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", [
      [ "Kutyatenyeszto.Repository.InstitutionRepository", "class_kutyatenyeszto_1_1_repository_1_1_institution_repository.html", null ]
    ] ],
    [ "Kutyatenyeszto.Repository.GenericRepository< User >", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", [
      [ "Kutyatenyeszto.Repository.UserRepository", "class_kutyatenyeszto_1_1_repository_1_1_user_repository.html", null ]
    ] ],
    [ "System.Windows.Markup.IComponentConnector", null, [
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.EditorWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.EditorWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.Window1", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_window1.html", null ],
      [ "Kutyatenyeszto.WPFRandom.ListWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.ListWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "Kutyatenyeszto.Logic.IDisplayLogic", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html", [
      [ "Kutyatenyeszto.Logic.DisplayLogic", "class_kutyatenyeszto_1_1_logic_1_1_display_logic.html", null ]
    ] ],
    [ "Kutyatenyeszto.WPF.BL.IEditorService", "interface_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_i_editor_service.html", [
      [ "Kutyatenyeszto.WPF.UI.EditorServiceViaWindows", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_service_via_windows.html", null ]
    ] ],
    [ "IEntityTypeConfiguration", null, [
      [ "Kutyatenyeszto.Data.EntityConfigurations.AdoptationConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_adoptation_configurations.html", null ],
      [ "Kutyatenyeszto.Data.EntityConfigurations.DogConfiguartions", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_dog_configuartions.html", null ],
      [ "Kutyatenyeszto.Data.EntityConfigurations.InstitutionConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_institution_configurations.html", null ],
      [ "Kutyatenyeszto.Data.EntityConfigurations.ServiceConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_service_configurations.html", null ],
      [ "Kutyatenyeszto.Data.EntityConfigurations.UserConfigurations", "class_kutyatenyeszto_1_1_data_1_1_entity_configurations_1_1_user_configurations.html", null ]
    ] ],
    [ "Kutyatenyeszto.WpfClient.IMainLogic", "interface_kutyatenyeszto_1_1_wpf_client_1_1_i_main_logic.html", [
      [ "Kutyatenyeszto.WpfClient.MainLogic", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_logic.html", null ]
    ] ],
    [ "Kutyatenyeszto.Logic.IManagementLogic", "interface_kutyatenyeszto_1_1_logic_1_1_i_management_logic.html", [
      [ "Kutyatenyeszto.Logic.ManagementLogic", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html", null ]
    ] ],
    [ "Kutyatenyeszto.Data.Models.Institution", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html", null ],
    [ "Kutyatenyeszto.WEB.Models.Institution", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html", null ],
    [ "Kutyatenyeszto.WEB.Models.InstitutionListViewModel", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution_list_view_model.html", null ],
    [ "System.Windows.Markup.InternalTypeHelper", null, [
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ],
      [ "XamlGeneratedNamespace.GeneratedInternalTypeHelper", "class_xaml_generated_namespace_1_1_generated_internal_type_helper.html", null ]
    ] ],
    [ "Kutyatenyeszto.Repository.IRepository< T >", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", [
      [ "Kutyatenyeszto.Repository.GenericRepository< T >", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", null ]
    ] ],
    [ "Kutyatenyeszto.Repository.IRepository< Adoptation >", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", [
      [ "Kutyatenyeszto.Repository.IAdoptationRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_adoptation_repository.html", [
        [ "Kutyatenyeszto.Repository.AdoptationRepository", "class_kutyatenyeszto_1_1_repository_1_1_adoptation_repository.html", null ]
      ] ]
    ] ],
    [ "Kutyatenyeszto.Repository.IRepository< Dog >", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", [
      [ "Kutyatenyeszto.Repository.IDogRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_dog_repository.html", [
        [ "Kutyatenyeszto.Repository.DogRepository", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html", null ]
      ] ]
    ] ],
    [ "Kutyatenyeszto.Repository.IRepository< Institution >", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", [
      [ "Kutyatenyeszto.Repository.IInstitutionRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_institution_repository.html", [
        [ "Kutyatenyeszto.Repository.InstitutionRepository", "class_kutyatenyeszto_1_1_repository_1_1_institution_repository.html", null ]
      ] ]
    ] ],
    [ "Kutyatenyeszto.Repository.IRepository< User >", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", [
      [ "Kutyatenyeszto.Repository.IUserRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_user_repository.html", [
        [ "Kutyatenyeszto.Repository.UserRepository", "class_kutyatenyeszto_1_1_repository_1_1_user_repository.html", null ]
      ] ]
    ] ],
    [ "IServiceLocator", null, [
      [ "Kutyatenyeszto.WPF.MyIoc", "class_kutyatenyeszto_1_1_w_p_f_1_1_my_ioc.html", null ],
      [ "Kutyatenyeszto.WpfClient.MyIoc", "class_kutyatenyeszto_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "Kutyatenyeszto.WPF.BL.IUserLogic", "interface_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_i_user_logic.html", [
      [ "Kutyatenyeszto.WPF.BL.UserLogic", "class_kutyatenyeszto_1_1_w_p_f_1_1_b_l_1_1_user_logic.html", null ]
    ] ],
    [ "Kutyatenyeszto.Repository.IWorkWithDatabase", "interface_kutyatenyeszto_1_1_repository_1_1_i_work_with_database.html", [
      [ "Kutyatenyeszto.Repository.WorkwithDatabase", "class_kutyatenyeszto_1_1_repository_1_1_workwith_database.html", null ]
    ] ],
    [ "Kutyatenyeszto.WPFRandom.MainLogic", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_logic.html", null ],
    [ "Kutyatenyeszto.Logic.Tests.ManagementLogicTest", "class_kutyatenyeszto_1_1_logic_1_1_tests_1_1_management_logic_test.html", null ],
    [ "Kutyatenyeszto.WEB.Models.MapperFactory", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_mapper_factory.html", null ],
    [ "Kutyatenyeszto.WEB.Controllers.NewApiResult", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_new_api_result.html", null ],
    [ "Kutyatenyeszto.WPFRandom.NewApiResult", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_new_api_result.html", null ],
    [ "ObservableObject", null, [
      [ "Kutyatenyeszto.WPF.Data.UserModel", "class_kutyatenyeszto_1_1_w_p_f_1_1_data_1_1_user_model.html", null ],
      [ "Kutyatenyeszto.WPFRandom.VM.InstitutionVM", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_institution_v_m.html", null ],
      [ "Kutyatenyeszto.WPFRandom.VM.MainVM", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_main_v_m.html", null ],
      [ "Kutyatenyeszto.WpfClient.InstitutionVM", "class_kutyatenyeszto_1_1_wpf_client_1_1_institution_v_m.html", null ]
    ] ],
    [ "Kutyatenyeszto.Program", "class_kutyatenyeszto_1_1_program.html", null ],
    [ "Kutyatenyeszto.WEB.Program", "class_kutyatenyeszto_1_1_w_e_b_1_1_program.html", null ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage", null, [
      [ "AspNetCore.Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", null ],
      [ "AspNetCore.Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", null ],
      [ "AspNetCore.Views_Institutions_InstitutionDetails", "class_asp_net_core_1_1_views___institutions___institution_details.html", null ],
      [ "AspNetCore.Views_Institutions_InstitutionEdit", "class_asp_net_core_1_1_views___institutions___institution_edit.html", null ],
      [ "AspNetCore.Views_Institutions_InstitutionIndex", "class_asp_net_core_1_1_views___institutions___institution_index.html", null ],
      [ "AspNetCore.Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", null ],
      [ "AspNetCore.Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", null ],
      [ "AspNetCore.Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", null ],
      [ "AspNetCore.Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", null ],
      [ "AspNetCore.Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Kutyatenyeszto.Data.Models.Institution >>", null, [
      [ "AspNetCore.Views_Random_SelectionResult", "class_asp_net_core_1_1_views___random___selection_result.html", null ]
    ] ],
    [ "global.MicrosoftAspNetCore.Mvc.Razor.RazorPage< IEnumerable< Kutyatenyeszto.WEB.Models.Institution >>", null, [
      [ "AspNetCore.Views_Institutions_InstitutionList", "class_asp_net_core_1_1_views___institutions___institution_list.html", null ]
    ] ],
    [ "Kutyatenyeszto.Properties.Resources", "class_kutyatenyeszto_1_1_properties_1_1_resources.html", null ],
    [ "Kutyatenyeszto.Data.Models.Service", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html", null ],
    [ "SimpleIoc", null, [
      [ "Kutyatenyeszto.WPF.MyIoc", "class_kutyatenyeszto_1_1_w_p_f_1_1_my_ioc.html", null ],
      [ "Kutyatenyeszto.WpfClient.MyIoc", "class_kutyatenyeszto_1_1_wpf_client_1_1_my_ioc.html", null ]
    ] ],
    [ "Kutyatenyeszto.WEB.Startup", "class_kutyatenyeszto_1_1_w_e_b_1_1_startup.html", null ],
    [ "Kutyatenyeszto.Data.Models.User", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html", null ],
    [ "Kutyatenyeszto.Logic.UserNamesWithIdSum", "class_kutyatenyeszto_1_1_logic_1_1_user_names_with_id_sum.html", null ],
    [ "ViewModelBase", null, [
      [ "Kutyatenyeszto.WPF.VM.EditorViewModel", "class_kutyatenyeszto_1_1_w_p_f_1_1_v_m_1_1_editor_view_model.html", null ],
      [ "Kutyatenyeszto.WPF.VM.MainViewModel", "class_kutyatenyeszto_1_1_w_p_f_1_1_v_m_1_1_main_view_model.html", null ],
      [ "Kutyatenyeszto.WPFRandom.VM.ListVM", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_list_v_m.html", null ],
      [ "Kutyatenyeszto.WpfClient.MainVM", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_v_m.html", null ]
    ] ],
    [ "System.Windows.Window", null, [
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.EditorWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.EditorWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.EditorWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WPF.UI.Window1", "class_kutyatenyeszto_1_1_w_p_f_1_1_u_i_1_1_window1.html", null ],
      [ "Kutyatenyeszto.WPFRandom.ListWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.ListWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.ListWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_list_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WPFRandom.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_random_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", null ],
      [ "Kutyatenyeszto.WpfClient.MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", null ]
    ] ],
    [ "Window", null, [
      [ "Kutyatenyeszto.WPF.MainWindow", "class_kutyatenyeszto_1_1_w_p_f_1_1_main_window.html", null ]
    ] ]
];