var namespace_kutyatenyeszto_1_1_wpf_client =
[
    [ "App", "class_kutyatenyeszto_1_1_wpf_client_1_1_app.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_app" ],
    [ "EditorWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_editor_window" ],
    [ "IMainLogic", "interface_kutyatenyeszto_1_1_wpf_client_1_1_i_main_logic.html", "interface_kutyatenyeszto_1_1_wpf_client_1_1_i_main_logic" ],
    [ "InstitutionVM", "class_kutyatenyeszto_1_1_wpf_client_1_1_institution_v_m.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_institution_v_m" ],
    [ "MainLogic", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_logic.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_logic" ],
    [ "MainVM", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_v_m.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_v_m" ],
    [ "MainWindow", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_main_window" ],
    [ "MyIoc", "class_kutyatenyeszto_1_1_wpf_client_1_1_my_ioc.html", "class_kutyatenyeszto_1_1_wpf_client_1_1_my_ioc" ]
];