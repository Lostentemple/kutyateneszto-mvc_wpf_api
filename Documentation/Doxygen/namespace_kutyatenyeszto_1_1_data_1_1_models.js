var namespace_kutyatenyeszto_1_1_data_1_1_models =
[
    [ "Adoptation", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation" ],
    [ "Dog", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog" ],
    [ "Institution", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution" ],
    [ "Service", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_service" ],
    [ "User", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user.html", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_user" ],
    [ "Furtypes", "namespace_kutyatenyeszto_1_1_data_1_1_models.html#aecd0879c79dbbf0c8935a51fc9805361", [
      [ "Shorthair", "namespace_kutyatenyeszto_1_1_data_1_1_models.html#aecd0879c79dbbf0c8935a51fc9805361a93c2a468e582af9cbbb00961f5326a15", null ],
      [ "Longhair", "namespace_kutyatenyeszto_1_1_data_1_1_models.html#aecd0879c79dbbf0c8935a51fc9805361a35ed0ed7b4bb8ad00472eb45766f8b88", null ]
    ] ],
    [ "GenderTypes", "namespace_kutyatenyeszto_1_1_data_1_1_models.html#a0dd60030bfb39e61faee7359b3b15d1b", [
      [ "Male", "namespace_kutyatenyeszto_1_1_data_1_1_models.html#a0dd60030bfb39e61faee7359b3b15d1ba63889cfb9d3cbe05d1bd2be5cc9953fd", null ],
      [ "Female", "namespace_kutyatenyeszto_1_1_data_1_1_models.html#a0dd60030bfb39e61faee7359b3b15d1bab719ce180ec7bd9641fece2f920f4817", null ]
    ] ]
];