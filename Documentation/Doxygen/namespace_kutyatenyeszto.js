var namespace_kutyatenyeszto =
[
    [ "Data", "namespace_kutyatenyeszto_1_1_data.html", "namespace_kutyatenyeszto_1_1_data" ],
    [ "Logic", "namespace_kutyatenyeszto_1_1_logic.html", "namespace_kutyatenyeszto_1_1_logic" ],
    [ "MenuActions", "namespace_kutyatenyeszto_1_1_menu_actions.html", "namespace_kutyatenyeszto_1_1_menu_actions" ],
    [ "Properties", "namespace_kutyatenyeszto_1_1_properties.html", "namespace_kutyatenyeszto_1_1_properties" ],
    [ "Repository", "namespace_kutyatenyeszto_1_1_repository.html", "namespace_kutyatenyeszto_1_1_repository" ],
    [ "WEB", "namespace_kutyatenyeszto_1_1_w_e_b.html", "namespace_kutyatenyeszto_1_1_w_e_b" ],
    [ "WPF", "namespace_kutyatenyeszto_1_1_w_p_f.html", "namespace_kutyatenyeszto_1_1_w_p_f" ],
    [ "WpfClient", "namespace_kutyatenyeszto_1_1_wpf_client.html", "namespace_kutyatenyeszto_1_1_wpf_client" ],
    [ "WPFRandom", "namespace_kutyatenyeszto_1_1_w_p_f_random.html", "namespace_kutyatenyeszto_1_1_w_p_f_random" ],
    [ "Program", "class_kutyatenyeszto_1_1_program.html", null ]
];