var class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution =
[
    [ "Address", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a6a7e389f15a895868414eaa29bacb4a1", null ],
    [ "Capacity", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a84fd5a151c226ba6dd701149f6f459e0", null ],
    [ "Director", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a3edeadda73b5abd3414ca3a5f2f3e67c", null ],
    [ "InstitutionId", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a8cb7408e4a30deda0df38633628d3e71", null ],
    [ "Name", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a7a7970ae39161e5aea4b84810bea1d85", null ],
    [ "PhoneNumber", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#ae4e64fe76fadde5efeef24790b1aed60", null ],
    [ "Selected", "class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a4792bdb5b1c04c01214e194dff16c26b", null ]
];