var class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context =
[
    [ "KutyatenyesztoDbContext", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#ab879758c61ea9ac0c4840338c90f02f6", null ],
    [ "OnConfiguring", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a775343488a48d7cdadd057a91311029f", null ],
    [ "OnModelCreating", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a62bc711bfdaa2aa2c6e7876dc3621f33", null ],
    [ "Adoptations", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a33bc1ef001bfded453d252f541029ad4", null ],
    [ "Dogs", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a02aeadb7d96d84867ea5dccb6ef75c53", null ],
    [ "Institutions", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a2318d0efb102b0eb366f249cb3387c3b", null ],
    [ "SingletonDbContext", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a1dded642bab4a2a6b660e49eab2b4fd3", null ],
    [ "Users", "class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#ab1a1705e75469d71149702630242c258", null ]
];