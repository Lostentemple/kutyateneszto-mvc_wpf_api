var class_kutyatenyeszto_1_1_repository_1_1_dog_repository =
[
    [ "DogRepository", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#a59981d22598d1d25bae888e615f6b4df", null ],
    [ "Delete", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#a30e8c70ce4d24d1790acc559ea7643fa", null ],
    [ "DeleteMore", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#aaf2fc6108bc79414230a9d111efe4174", null ],
    [ "GetById", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#a844715d7c80fd682adfcda12e1b5aeab", null ],
    [ "Insert", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#abcd8b5e10aaf6e53df421359774ac5a6", null ],
    [ "InsertMore", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#aa2bc3f4aad3abbb3894b583c45887f5d", null ],
    [ "Update", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html#a8dbed66fc4284c5faeb9307f4bea1e4b", null ]
];