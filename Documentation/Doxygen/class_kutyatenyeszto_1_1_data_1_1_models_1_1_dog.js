var class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog =
[
    [ "Age", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#a354a34b5321721fb8dbedb290cc11b8d", null ],
    [ "Breed", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#aec60863ea41d5c8f2da34abc2c793c88", null ],
    [ "DogId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#ad95e1944a5d6a42f55420bc44248f442", null ],
    [ "FurType", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#ade12106def5c23974d22f3ca14e80609", null ],
    [ "HasOwner", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#a6ac5b8614db64c042b38dfe8f4e9afc3", null ],
    [ "IsAccessible", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#a419a9707672b93952a1e39b84df554be", null ],
    [ "IsUncastrated", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#a07e632e494a6c36cc42af59f1116d62b", null ],
    [ "Name", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_dog.html#a13afca0bad4c568c7cfc76eaf4fb1253", null ]
];