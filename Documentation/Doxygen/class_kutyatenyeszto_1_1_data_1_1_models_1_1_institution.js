var class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution =
[
    [ "Address", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#ae6c515a2605229747ef3ab2db9158ad5", null ],
    [ "Capacity", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a34fbadd60f4ff868baf50144f306c89f", null ],
    [ "Director", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a77096ba481287cbac5de23f7af1433cb", null ],
    [ "InstitutionId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#aada60b52d87953f4bc6ed32a0108d3e6", null ],
    [ "Name", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a5429dcf949998f75fbc22a148b0f8759", null ],
    [ "PhoneNumber", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a1f56b84345dd7290b0f15ba658495084", null ],
    [ "Selected", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a0ffe9f378523db378a07bbe0301c1e0f", null ]
];