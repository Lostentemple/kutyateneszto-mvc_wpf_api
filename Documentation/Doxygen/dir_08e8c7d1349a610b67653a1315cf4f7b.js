var dir_08e8c7d1349a610b67653a1315cf4f7b =
[
    [ "Kutyatenyeszto", "dir_4dc8d06dac66de2d14918ef650a50224.html", "dir_4dc8d06dac66de2d14918ef650a50224" ],
    [ "Kutyatenyeszto.Data", "dir_87c5d8bceb6856e91c370815476c2fed.html", "dir_87c5d8bceb6856e91c370815476c2fed" ],
    [ "Kutyatenyeszto.Logic", "dir_3269712c7b5305817662f63269445cfe.html", "dir_3269712c7b5305817662f63269445cfe" ],
    [ "Kutyatenyeszto.Logic.Tests", "dir_2518b88d09813e6ec60346674af8e87d.html", "dir_2518b88d09813e6ec60346674af8e87d" ],
    [ "Kutyatenyeszto.Repository", "dir_ee0699fd17a838cb41bb99133230b1aa.html", "dir_ee0699fd17a838cb41bb99133230b1aa" ],
    [ "Kutyatenyeszto.WEB", "dir_e16659762292a94b86f6cc7adc71bc5b.html", "dir_e16659762292a94b86f6cc7adc71bc5b" ],
    [ "Kutyatenyeszto.WPF", "dir_1c3d15691090f5cbfd3ba03a105d2c83.html", "dir_1c3d15691090f5cbfd3ba03a105d2c83" ],
    [ "Kutyatenyeszto.WpfClient", "dir_000147876ecc032f03458b1707a29a93.html", "dir_000147876ecc032f03458b1707a29a93" ],
    [ "Kutyatenyeszto.WPFRandom", "dir_b7a9810e43d8fc7d36adae5889602175.html", "dir_b7a9810e43d8fc7d36adae5889602175" ]
];