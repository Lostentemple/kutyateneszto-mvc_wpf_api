var class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation =
[
    [ "AdoptationId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#abaa64328f42d628dec5448d1fea8302b", null ],
    [ "Dog", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#adae9603e846d0ebafeede188867d2c30", null ],
    [ "DogId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#a7dbe4ebae33d6cbeb4bdc5f9222a1d69", null ],
    [ "Institution", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#adf14c8019e66ae9848f35a1a40b264a4", null ],
    [ "InstitutionId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#a0c7a1a2c5b4868e5b7775f91231ad503", null ],
    [ "User", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#af943adc1848f833231a1081e716ec56c", null ],
    [ "UserId", "class_kutyatenyeszto_1_1_data_1_1_models_1_1_adoptation.html#a6435df26e1edf8c92e7940bcb171717e", null ]
];