var namespace_kutyatenyeszto_1_1_repository =
[
    [ "AdoptationRepository", "class_kutyatenyeszto_1_1_repository_1_1_adoptation_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_adoptation_repository" ],
    [ "DogRepository", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_dog_repository" ],
    [ "GenericRepository", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_generic_repository" ],
    [ "IAdoptationRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_adoptation_repository.html", null ],
    [ "IDogRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_dog_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_dog_repository" ],
    [ "IInstitutionRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_institution_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_institution_repository" ],
    [ "InstitutionRepository", "class_kutyatenyeszto_1_1_repository_1_1_institution_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_institution_repository" ],
    [ "IRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_repository" ],
    [ "IUserRepository", "interface_kutyatenyeszto_1_1_repository_1_1_i_user_repository.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_user_repository" ],
    [ "IWorkWithDatabase", "interface_kutyatenyeszto_1_1_repository_1_1_i_work_with_database.html", "interface_kutyatenyeszto_1_1_repository_1_1_i_work_with_database" ],
    [ "UserRepository", "class_kutyatenyeszto_1_1_repository_1_1_user_repository.html", "class_kutyatenyeszto_1_1_repository_1_1_user_repository" ],
    [ "WorkwithDatabase", "class_kutyatenyeszto_1_1_repository_1_1_workwith_database.html", "class_kutyatenyeszto_1_1_repository_1_1_workwith_database" ]
];