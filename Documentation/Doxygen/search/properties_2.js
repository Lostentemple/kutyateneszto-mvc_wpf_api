var searchData=
[
  ['capacity_477',['Capacity',['../class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a34fbadd60f4ff868baf50144f306c89f',1,'Kutyatenyeszto.Data.Models.Institution.Capacity()'],['../class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a84fd5a151c226ba6dd701149f6f459e0',1,'Kutyatenyeszto.WEB.Models.Institution.Capacity()'],['../class_kutyatenyeszto_1_1_wpf_client_1_1_institution_v_m.html#a81e4710acce7ec1cfeae4f381653d3ed',1,'Kutyatenyeszto.WpfClient.InstitutionVM.Capacity()'],['../class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_institution_v_m.html#a9b983f8c90349bd03ec290c138a1f27a',1,'Kutyatenyeszto.WPFRandom.VM.InstitutionVM.Capacity()']]],
  ['configuration_478',['Configuration',['../class_kutyatenyeszto_1_1_w_e_b_1_1_startup.html#accabe34bb720058072d7215f8241ae25',1,'Kutyatenyeszto::WEB::Startup']]],
  ['context_479',['Context',['../class_kutyatenyeszto_1_1_repository_1_1_workwith_database.html#a4d695e75ecaf73626c38535e354ba41d',1,'Kutyatenyeszto::Repository::WorkwithDatabase']]],
  ['count_480',['Count',['../class_kutyatenyeszto_1_1_logic_1_1_available_dog.html#aea55ccc3e9c292309ea28d7f1d67dc87',1,'Kutyatenyeszto.Logic.AvailableDog.Count()'],['../class_kutyatenyeszto_1_1_w_p_f_random_1_1_new_api_result.html#aa14bb3c5182da123e6d810e783fc70c8',1,'Kutyatenyeszto.WPFRandom.NewApiResult.Count()']]],
  ['culture_481',['Culture',['../class_kutyatenyeszto_1_1_properties_1_1_resources.html#a8e15f4a730add4abd4b6df8c4f7b7522',1,'Kutyatenyeszto::Properties::Resources']]]
];
