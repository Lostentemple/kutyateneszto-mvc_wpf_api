var searchData=
[
  ['selected_521',['Selected',['../class_kutyatenyeszto_1_1_data_1_1_models_1_1_institution.html#a0ffe9f378523db378a07bbe0301c1e0f',1,'Kutyatenyeszto.Data.Models.Institution.Selected()'],['../class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_institution.html#a4792bdb5b1c04c01214e194dff16c26b',1,'Kutyatenyeszto.WEB.Models.Institution.Selected()'],['../class_kutyatenyeszto_1_1_w_p_f_random_1_1_v_m_1_1_institution_v_m.html#a28e372722b05713ee789d0906a53a606',1,'Kutyatenyeszto.WPFRandom.VM.InstitutionVM.Selected()']]],
  ['selectedcount_522',['SelectedCount',['../class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_new_api_result.html#a7869ff3d5b83acc18096b63b39e81b20',1,'Kutyatenyeszto::WEB::Controllers::NewApiResult']]],
  ['selectedinstitution_523',['SelectedInstitution',['../class_kutyatenyeszto_1_1_wpf_client_1_1_main_v_m.html#a6fea58a6465238178861b98879a027fe',1,'Kutyatenyeszto::WpfClient::MainVM']]],
  ['servicedate_524',['ServiceDate',['../class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html#adb07f2df6d8e13ed654df117aa47049b',1,'Kutyatenyeszto::Data::Models::Service']]],
  ['servicename_525',['ServiceName',['../class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html#a36779a413c508614735ead36c35c0ed5',1,'Kutyatenyeszto::Data::Models::Service']]],
  ['serviceprovider_526',['ServiceProvider',['../class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html#a7aea918ff32748617fbffad792a255d3',1,'Kutyatenyeszto::Data::Models::Service']]],
  ['servicesid_527',['ServicesId',['../class_kutyatenyeszto_1_1_data_1_1_models_1_1_service.html#ab3b3ac0f68261152a594608feee8dfc4',1,'Kutyatenyeszto::Data::Models::Service']]],
  ['showrequestid_528',['ShowRequestId',['../class_kutyatenyeszto_1_1_w_e_b_1_1_models_1_1_error_view_model.html#a9a4cafa86e392eeaaa1a1cf948145fd9',1,'Kutyatenyeszto::WEB::Models::ErrorViewModel']]],
  ['singletondbcontext_529',['SingletonDbContext',['../class_kutyatenyeszto_1_1_data_1_1_kutyatenyeszto_db_context.html#a1dded642bab4a2a6b660e49eab2b4fd3',1,'Kutyatenyeszto::Data::KutyatenyesztoDbContext']]],
  ['successmsg_530',['successMsg',['../class_kutyatenyeszto_1_1_properties_1_1_resources.html#afd38e43023ca65a055e1cef5028f73f5',1,'Kutyatenyeszto::Properties::Resources']]],
  ['sum_531',['Sum',['../class_kutyatenyeszto_1_1_logic_1_1_user_names_with_id_sum.html#aabd94a611b6938eda717fd6b48951450',1,'Kutyatenyeszto::Logic::UserNamesWithIdSum']]]
];
