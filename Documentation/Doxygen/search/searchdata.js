var indexSectionsWithContent =
{
  0: "abcdefghiklmnoprstuvwx",
  1: "acdeghiklmnprsuvw",
  2: "akx",
  3: "acdeghiklmnoprsuw",
  4: "c",
  5: "fg",
  6: "flms",
  7: "abcdefghilmnoprstu",
  8: "t"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "functions",
  4: "variables",
  5: "enums",
  6: "enumvalues",
  7: "properties",
  8: "pages"
};

var indexSectionLabels =
{
  0: "All",
  1: "Classes",
  2: "Namespaces",
  3: "Functions",
  4: "Variables",
  5: "Enumerations",
  6: "Enumerator",
  7: "Properties",
  8: "Pages"
};

