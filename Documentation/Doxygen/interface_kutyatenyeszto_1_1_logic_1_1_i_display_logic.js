var interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic =
[
    [ "GetAllDogs", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html#a9faec20036ee6aa3ce6107fe41761bf4", null ],
    [ "GetAllInstitutions", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html#acbaf308e9017f0c1cbd561bd4148b74a", null ],
    [ "GetAllUsers", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html#af643037b76a951538483e28504b5d5d6", null ],
    [ "GetOneDog", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html#a8bde4226f9f02ba30015e396d96a1348", null ],
    [ "GetOneInstitution", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html#ad5618f0408e42c8dc1507c1bc840e2d7", null ],
    [ "GetOneUser", "interface_kutyatenyeszto_1_1_logic_1_1_i_display_logic.html#ae86802baf0e1d960c446e265a75fb9df", null ]
];