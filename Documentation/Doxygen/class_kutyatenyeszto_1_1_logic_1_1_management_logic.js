var class_kutyatenyeszto_1_1_logic_1_1_management_logic =
[
    [ "ManagementLogic", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a594c73def0c41b8a218804473c9b2030", null ],
    [ "DeleteDog", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a561fd966b1c0a33417600accc8885ede", null ],
    [ "DeleteInstitution", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a40cf09befd43c23008a718b8d65bad67", null ],
    [ "DeleteUser", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a16f5bd2614c893d4766be419b40942ff", null ],
    [ "InsertDog", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#ad7887434b94e7573c9c7ce5233c23950", null ],
    [ "InsertInstitution", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#af585d07a1a4cf4dfb170792304df6deb", null ],
    [ "InsertUser", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a88f46a311718afc57654b592fbe60b9e", null ],
    [ "UpdateDog", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a5fd3065bc9f777fa60f48c3f95ede69c", null ],
    [ "UpdateInstitution", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#aceac4fa9d6ea5d688c6c92e363d90f40", null ],
    [ "UpdateUser", "class_kutyatenyeszto_1_1_logic_1_1_management_logic.html#a9f8a326edae1b341db8363e7de7252ed", null ]
];