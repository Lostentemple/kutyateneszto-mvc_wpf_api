var namespace_asp_net_core =
[
    [ "Views__ViewImports", "class_asp_net_core_1_1_views_____view_imports.html", "class_asp_net_core_1_1_views_____view_imports" ],
    [ "Views__ViewStart", "class_asp_net_core_1_1_views_____view_start.html", "class_asp_net_core_1_1_views_____view_start" ],
    [ "Views_Home_Index", "class_asp_net_core_1_1_views___home___index.html", "class_asp_net_core_1_1_views___home___index" ],
    [ "Views_Home_Privacy", "class_asp_net_core_1_1_views___home___privacy.html", "class_asp_net_core_1_1_views___home___privacy" ],
    [ "Views_Institutions_InstitutionDetails", "class_asp_net_core_1_1_views___institutions___institution_details.html", "class_asp_net_core_1_1_views___institutions___institution_details" ],
    [ "Views_Institutions_InstitutionEdit", "class_asp_net_core_1_1_views___institutions___institution_edit.html", "class_asp_net_core_1_1_views___institutions___institution_edit" ],
    [ "Views_Institutions_InstitutionIndex", "class_asp_net_core_1_1_views___institutions___institution_index.html", "class_asp_net_core_1_1_views___institutions___institution_index" ],
    [ "Views_Institutions_InstitutionList", "class_asp_net_core_1_1_views___institutions___institution_list.html", "class_asp_net_core_1_1_views___institutions___institution_list" ],
    [ "Views_Random_SelectionResult", "class_asp_net_core_1_1_views___random___selection_result.html", "class_asp_net_core_1_1_views___random___selection_result" ],
    [ "Views_Shared__Layout", "class_asp_net_core_1_1_views___shared_____layout.html", "class_asp_net_core_1_1_views___shared_____layout" ],
    [ "Views_Shared__ValidationScriptsPartial", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial.html", "class_asp_net_core_1_1_views___shared_____validation_scripts_partial" ],
    [ "Views_Shared_Error", "class_asp_net_core_1_1_views___shared___error.html", "class_asp_net_core_1_1_views___shared___error" ]
];