var namespace_kutyatenyeszto_1_1_w_e_b_1_1_controllers =
[
    [ "ApiResult", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_api_result.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_api_result" ],
    [ "HomeController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_home_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_home_controller" ],
    [ "InstitutionsApiController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_api_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_api_controller" ],
    [ "InstitutionsController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_institutions_controller" ],
    [ "NewApiResult", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_new_api_result.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_new_api_result" ],
    [ "RandomController", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_random_controller.html", "class_kutyatenyeszto_1_1_w_e_b_1_1_controllers_1_1_random_controller" ]
];